var tblCategories = $("#tblCategories");
var tblCategoriesColumns = $("#tblCategories th");
var tblCategoriesContent = $("#tblCategories tbody");
var modalBtnNew = $("#modal-btn-new");

var category_id = $("[name='category_id']");
var inputCategoryCode = $("[name='inputCategoryCode']");
var inputCategoryName = $("[name='inputCategoryName']");
var inputCategoryEquivalent = $("[name='inputCategoryEquivalent']");

/** Start Populate main Table List */
CATEGORIESpopulateMainTable(true, true);
function CATEGORIESpopulateMainTable(ini = false, showMessage = false) {
    $.ajax({
        type: "POST",
        url: "/CATEGORY-load-items",
        dataType: "json",
        success: function (response) {
            if (showMessage) {
                showToast(response.remarks, response.message);
            }
            var items = response.data.items;
            if (items.length > 0) {
                if (ini) {
                    // script for initial load only
                }
                return CATEGORIESpopulateTable(items);
            }
            return CATEGORIESpopulateTable(false);
        },
        error: function (xhr, status, error) {
            console.log(xhr);
            return tblCategories.append(
                `<tr>` +
                    `<td colspan="` +
                    tblCategoriesColumns.length +
                    `">` +
                    xhr.responseJSON.message +
                    `</td>` +
                    `</tr>`
            );
        },
    });
}
/** End Populate main Table List */
/** Start Function to populate the table view */
function CATEGORIESpopulateTable(items) {
    var rows = "";
    if ($.fn.DataTable.isDataTable("#" + tblCategories.attr("id"))) {
        tblCategories.DataTable().destroy();
    }

    tblCategoriesContent.empty();

    if (items) {
        if (items.length > 0) {
            var counter = 1;
            for (var i = 0; i < items.length; i++) {
                var data_id = items[i].id;
                var data_name = trimData(items[i].name);

                var btnView =
                    `<i class="fas fa-eye" title="View Record" aria-hidden="true" onclick="CATEGORIESfindById(` +
                    data_id +
                    `, true)"></i>`;
                var btnEdit =
                    ` / <i class="fas fa-edit" title="Edit Record" aria-hidden="true" onclick="CATEGORIESfindById(` +
                    data_id +
                    `, false)"></i>`;
                var btnDel =
                    ` / <i class="fas fa-trash" title="Remove Record" aria-hidden="true" onclick="deleteConfirmation(` +
                    data_id +
                    `,CATEGORIESDeleteById)"></i>`;
                rows += `<tr>`;
                rows += ` <td>` + counter + `</td>`; // NoNo.
                rows += ` <td>` + trimData(items[i].name) + `</td>`; // Client Name
                rows += ` <td>` + trimData(items[i].generator_code) + `</td>`; // generator_code
                rows += ` <td>` + trimData(items[i].equivalent_no) + `</td>`; // equivalent_no
                rows += ` <td>` + btnView + btnEdit + btnDel + `</td>`; // Actions
                rows += `</tr>`;
                counter++;
            }

            tblCategories.append(rows);
            return new DataTable("#" + tblCategories.attr("id"));
        }
    }
    return tblCategories.append(
        `<td colspan="` +
            tblCategoriesColumns.length +
            `">No data available...</td>`
    );
}
/** End Function to populate the table view */

/** Start Find by ID */
function CATEGORIESfindById(id, isReadOnly = false) {
    $.ajax({
        type: "POST",
        url: "/CATEGORY-load-item",
        data: {
            category_id: id,
        },
        dataType: "json",
        success: function (response) {
            showToast(response.remarks, response.message);
            var item = response.data.item;
            if (item) {
                category_id.val(item.id);
                inputCategoryName.val(item.name);
                inputCategoryCode.val(item.generator_code);
                inputCategoryEquivalent.val(item.equivalent_no);
                $("#modal-new").modal("show");
                console.log(item);
            }
        },
        error: function (xhr, status, error) {
            console.log(xhr);
            return showToast("error", "Internal Error!");
        },
    });
}
/** End Find by ID */

/** Start Delete by ID */
function CATEGORIESDeleteById(id) {
    $.ajax({
        type: "POST",
        url: "/CATEGORY-delete-data",
        data: {
            category_id: id,
        },
        dataType: "json",
        success: function (response) {
            CATEGORIESpopulateMainTable(false, false);
            showToast(response.remarks, response.message);
        },
        error: function (xhr, status, error) {
            console.log(xhr);
            return showToast("error", "Internal Error!");
        },
    });
}
/** End Delete by ID */

modalBtnNew.click(function (e) {
    category_id.val(0);
    inputCategoryCode.val("");
    inputCategoryName.val("");
    inputCategoryEquivalent.val("");
    $("#modal-new").modal("show");
});

$("#formNewCategory").submit(function (e) {
    e.preventDefault(e);
    var formData = $(this).serializeArray();

    //Add select data field as it will not included in formValidation
    //formData.push({ name: "client_id", value: client_id.val() });

    // return console.log(formData);
    if (formValidation(formData)) {
        $.ajax({
            type: "POST",
            url: "/CATEGORY-save-data",
            dataType: "json",
            data: formData,
            success: function (response) {
                if (response.remarks == "success") {
                    CATEGORIESpopulateMainTable(false, false);
                    $("#modal-new").modal("hide");
                } else if (response.remarks == "warning") {
                    //duplicate caught
                    var inputErr = response.data.inputErr;
                    if (inputErr.length > 0) {
                        for (var i = 0; i < inputErr.length; i++) {
                            var inputField = $("[name='" + inputErr[i] + "']");
                            inputField.addClass("is-invalid");
                            inputField.after(
                                `<span class="text-danger">Already taken.</span>`
                            );
                        }
                    }
                }
                return showToast(response.remarks, response.message);
            },
            error: function (xhr, status, error) {
                console.log(xhr);
                return showToast("error", "Internal Error!");
            },
        });
    }
});
