var tblTicketearnCode = $("#tblTicketearnCode");
var tblTicketearnCodeColumns = $("#tblTicketearnCode th");
var tblTicketearnCodeContent = $("#tblTicketearnCode tbody");
var modalBtnNew = $("#modal-btn-new");



var tblTicketearnCodeVerified = $("#tblTicketearnCodeVerified");
var tblTicketearnCodeColumnsVerified = $("#tblTicketearnCodeVerified th");
var tblTicketearnCodeContentVerified = $("#tblTicketearnCodeVerified tbody");






var employee_id = $("[name='employee_id']");
// var employee_id = $("[name='employee_id']");
var inputLastName = $("[name='inputLastName']");
var inputMiddleName = $("[name='inputMiddleName']");
var inputPhoneNo = $("[name='inputPhoneNo']");
var inputPosition = $("[name='inputPosition']");
var inputClientName = $("[name='inputClientName']");



var inputEmployee = $("[name='inputEmployee']");
var inputCategory = $("[name='inputCategory']");


$(document).ready(function() {
    // Add event listener for client selection
    $('#inputClientName').on('change', function() {
        var selectedClientId = $(this).val();
        var selectedEmployeeId = $('#EmployeeEarns').val();
        populateMainTable(selectedClientId, selectedEmployeeId);
    });

    // Add event listener for employee selection
    $('#EmployeeEarns').on('change', function() {
        var selectedClientId = $('#inputClientName').val();
        var selectedEmployeeId = $(this).val();
        populateMainTable(selectedClientId, selectedEmployeeId);
    });

    // Function to populate main table based on client and employee selection
    function populateMainTable(selectedClientId, selectedEmployeeId) {
        var requestData = {
            clientId: selectedClientId,
            employeeId: selectedEmployeeId
        };

        $.ajax({
            type: "POST",
            url: "/TICKETEARN-load-items",
            dataType: "json",
            data: requestData,
            success: function(response) {
                if (response.remarks === "success") {
                    // Data found, populate the table
                    TICKETEARNSpopulateTable(response.data.items);
                } else {
                    // No data found
                    TICKETEARNSpopulateTable([]);
                }
                showToast(response.remarks, response.message);
            },
            error: function(xhr, status, error) {
                console.log(xhr);
                showToast('error', xhr.responseJSON.message);
                TICKETEARNSpopulateTable([]);
            }
        });
    }

    // Initial population of main table
    populateMainTable(null, null);
});



/** End Populate main Table List */
/** Start Function to populate the table view */
function TICKETEARNSpopulateTable(items) {

    var rows = "";
    if ($.fn.DataTable.isDataTable("#" + tblTicketearnCode.attr("id"))) {
        tblTicketearnCode.DataTable().destroy();
    }

    tblTicketearnCodeContent.empty();

    if (items) {
        if (items.length > 0) {
            var counter = 1;
            for (var i = 0; i < items.length; i++) {
                var data_id = items[i].catid;
                var data_first_name = trimData(items[i].first_name);
                var data_last_name = trimData(items[i].last_name);
                var data_middle_name = trimData(items[i].middle_name);
                var data_verified_at = trimData(items[i].verified_at);
                var data_clientname = trimData(items[i].clientname);
                var data_categname = trimData(items[i].categname);
                var data_ticket_earn = trimData(items[i].ticket_earn);

                var data_AreasCode = trimData(items[i].areasCode);

                var data_ClientCode = trimData(items[i].clientCode);
                var data_CategoryCode = trimData(items[i].categCode);

                var data_created_at = new Date(items[i].created_at);
                var options = {
                    year: "numeric",
                    month: "long",
                    day: "numeric",
                };
                var formatted_created_at = data_created_at.toLocaleString(
                    "en-US",
                    options
                );
                var btnView =
                    `<i class="fas fa-eye" title="View Record" aria-hidden="true" onclick="TICKETEARNSfindById(` +
                    data_id +
                    `, true)"></i>`;
                var btnEdit =
                    ` / <i class="fas fa-edit" title="Edit Record" aria-hidden="true" onclick="TICKETEARNSfindById(` +
                    data_id +
                    `, false)"></i>`;
                var btnDel =
                    `  <i class="fas fa-trash" title="Remove Record" aria-hidden="true" onclick="deleteConfirmation(` +
                    data_id +
                    `,TICKETEARNSDeleteById)"></i>`;
                rows += `<tr>`;
                rows += ` <td>` + counter + `</td>`; // No
                rows += ` <td>` + data_clientname + `</td>`; // Total NEW PRF
                rows +=
                    ` <td>` +
                    data_last_name +
                    " " +
                    data_first_name +
                    ", " +
                    data_middle_name +
                    `</td>`; // Name

               
        rows +=  `<td>`
            if (data_verified_at == 1){
                rows += `<span class="badge bg-success">VERIFIED</span>`;
            } else{

                rows += `<span class="badge bg-danger">UNVERIFIED</span>`;
            } 
            
      rows += `</td>`;

                    
              
                
                rows += ` <td>` + data_categname + `</td>`; // Total Pending
                rows +=
                    ` <td>` +
                    data_AreasCode +
                    "-" +
                    data_ClientCode +
                    "-" +
                    data_CategoryCode +
                    "-" +
                    data_ticket_earn +
                    `</td>`; // Total Pending


                rows += ` <td>` + formatted_created_at + `</td>`; // Total Pending

               
                rows += ` <td> <input type="checkbox" name="selectedEmployees[]" value="`+data_id+`" /></td>`; 
                //rows += ` <td>` + btnDel + `</td>`; // Actions
                rows += `</tr>`;

                counter++;
            }
            tblTicketearnCode.append(rows);

            return new DataTable(tblTicketearnCode, {
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "lengthMenu": [10, 50,100,500] // Define the entries options
            });
            

            
        }
    }

}


/** Start Find by ID */
function TICKETEARNSfindById(data_id) {
    var currentStatus = $('#verification_icon_' + data_id + ' span').text().trim();
    var newStatus = currentStatus === 'VERIFIED' ? 'UNVERIFIED' : 'VERIFIED';

    $.ajax({
        type: "POST",
        url: "/TICKETEARN-update-data",
        data: {
            employee_id: data_id,
            new_status: newStatus
        },
        dataType: "json",
        success: function(response) {
            if (response.remarks === "success") {
                $('#verification_icon_' + data_id + ' span').text(newStatus);
                $('#verification_icon_' + data_id + ' span').toggleClass('bg-success bg-danger');
            } else {
                alert("Error: " + response.message);
            }
        },
        error: function(xhr, status, error) {
            console.error(xhr.responseText);
        }
    });
}
/** End Find by ID */



$(document).ready(function () {
    $.ajax({
        url: "/CATEGORY-load-mindata",
        type: "POST",
        dataType: "json",
        success: function (data) {
            var select = $("#Category");

            // Clear existing options
            select.empty();

            // Add default option or prompt if needed
            select.append("<option value=''>Select a Category</option>");

            // Populate dropdown with client data
            $.each(data.data.items, function (index, item) {
                select.append(
                    $("<option>", {
                        value: item.id,
                        text: "(" + item.equivalent_no + ")" + " " + item.name,
                    })
                );
            });
            // select.multiSelect({
            //     keepOrder: true, // If you want to keep the order of selection
            // });
        },
        error: function (xhr, status, error) {
            console.error("Error fetching Category:", error);
        },
    });
});







$(document).ready(function () {
    $.ajax({
        url: "/EMPLOYEE-load-mindata-earn",
        type: "POST",
        dataType: "json",
        success: function (data) {
            var select = $("#EmployeeEarn");

            // Clear existing options
            select.empty();

            // Add default option or prompt if needed
            select.append("<option value=''>Select a Employee</option>");

            // Populate dropdown with client data
            $.each(data.data.items, function (index, item) {
                select.append(
                    $("<option>", {
                        value: item.id,
                        text: item.first_name,
                    })
                );
            });
        },
        error: function (xhr, status, error) {
            console.error("Error fetching Employee:", error);
        },
    });

});






$(document).ready(function () {
    $.ajax({
        url: "/CLIENT-load-mindata",
        type: "POST",
        dataType: "json",
        success: function (data) {
            var select = $("#inputClientName");

            // Clear existing options
            select.empty();

            // Add default option or prompt if needed
            select.append("<option value=''>Select all Client</option>");

            // Populate dropdown with client data
            $.each(data.data.items, function (index, item) {
                select.append(
                    $("<option>", {
                        value: item.id,
                        text: item.name,
                    })
                );
            });
        },
        error: function (xhr, status, error) {
            console.error("Error fetching clients:", error);
        },
    });

    //
});




// Rest of your existing code
$("#inputClientName").on("change", function () {
    var selectedClientId = $(this).val();
    var employeeListContainer = $("#EmployeeEarns");
    employeeListContainer.empty(); // Clear existing content

    // Perform an AJAX request to get employees based on the selected client ID
    $.ajax({
        url: "/EMPLOYEE-load-mindata-earn",
        type: "POST",
        dataType: "json",
        data: { clientId: selectedClientId },
        success: function (employeeData) {
            // Handle the received employee data
            if (employeeData.remarks === "success") {
                // Store the original employee data for filtering
                var originalEmployeeData = employeeData.data.items;

                // Function to populate container with checkboxes for each employee
                function populateEmployeeList(data) {
                    $.each(data, function(index, employee) {
                        var fullName = employee.last_name + ', ' + employee.first_name + ' ' + employee.middle_name;
                        $('#EmployeeEarns').append($('<option>', {
                            value: employee.id,
                            text: fullName
                        }));
                    });
                
                }

                // Initial population
                populateEmployeeList(originalEmployeeData);
            } else {
                // Display a message or handle error case
                console.error(
                    "Error fetching employees:",
                    employeeData.message
                );
            }
        },
        error: function (xhr, status, error) {
            console.error("Error fetching employees:", error);
        },
    });
});









$(document).ready(function () {
    $.ajax({
        url: "/CLIENT-load-mindata",
        type: "POST",
        dataType: "json",
        success: function (data) {
            var select = $("#VinputClientName");

            // Clear existing options
            select.empty();

            // Add default option or prompt if needed
            select.append("<option value=''>Select all Client</option>");

            // Populate dropdown with client data
            $.each(data.data.items, function (index, item) {
                select.append(
                    $("<option>", {
                        value: item.id,
                        text: item.name,
                    })
                );
            });
        },
        error: function (xhr, status, error) {
            console.error("Error fetching clients:", error);
        },
    });

    //
});




// Rest of your existing code
$("#VinputClientName").on("change", function () {
    var selectedClientId = $(this).val();
    var employeeListContainer = $("#VEmployeeEarns");
    employeeListContainer.empty(); // Clear existing content

    // Perform an AJAX request to get employees based on the selected client ID
    $.ajax({
        url: "/EMPLOYEE-load-mindata-earn",
        type: "POST",
        dataType: "json",
        data: { clientId: selectedClientId },
        success: function (employeeData) {
            // Handle the received employee data
            if (employeeData.remarks === "success") {
                // Store the original employee data for filtering
                var originalEmployeeData = employeeData.data.items;

                // Function to populate container with checkboxes for each employee
                function VerifiedpopulateMainTable(data) {
                    $.each(data, function(index, employee) {
                        var fullName = employee.last_name + ', ' + employee.first_name + ' ' + employee.middle_name;
                        $('#VEmployeeEarns').append($('<option>', {
                            value: employee.id,
                            text: fullName
                        }));
                    });
                
                }

                // Initial population
                VerifiedpopulateMainTable(originalEmployeeData);
            } else {
                // Display a message or handle error case
                console.error(
                    "Error fetching employees:",
                    employeeData.message
                );
            }
        },
        error: function (xhr, status, error) {
            console.error("Error fetching employees:", error);
        },
    });
});





$(document).ready(function() {
    // Add event listener for client selection
    $('#VinputClientName').on('change', function() {
        var vselectedClientId = $(this).val();
        var vselectedEmployeeId = $('#VEmployeeEarns').val();
        VerifiedpopulateMainTable(vselectedClientId, vselectedEmployeeId);
    });

    // Add event listener for employee selection
    $('#VEmployeeEarns').on('change', function() {
        var vselectedClientId = $('#VinputClientName').val();
        var vselectedEmployeeId = $(this).val();
        VerifiedpopulateMainTable(vselectedClientId, vselectedEmployeeId);
    });




 function VerifiedpopulateMainTable(vselectedClientId, vselectedEmployeeId) {
            var requestDatas = {
                clientId: vselectedClientId,
                employeeId: vselectedEmployeeId
            };

    $.ajax({
        type: "POST",
        url: "/TICKETEARN-load-verified-items",
        dataType: "json",
        data: requestDatas,
        success: function(response) {
            if (response.remarks === "success") {
                // Data found, populate the table
                VeifiriedpopulateTable(response.data.items);
            } else {
                // No data found
                VeifiriedpopulateTable([]);
            }
            showToast(response.remarks, response.message);
        },
        error: function(xhr, status, error) {
            console.log(xhr);
            showToast('error', xhr.responseJSON.message);
            VeifiriedpopulateTable([]);
        }

    });
}
});

/** End Populate main Table List */
/** Start Function to populate the table view */
function VeifiriedpopulateTable(items) {
    var rows = "";
    if ($.fn.DataTable.isDataTable("#" + tblTicketearnCodeVerified.attr("id"))) {
        tblTicketearnCodeVerified.DataTable().destroy();
    }

    tblTicketearnCodeContentVerified.empty();

    if (items) {
        if (items.length > 0) 
        var counter = 1;
        for (var i = 0; i < items.length; i++) {
            var data_id = items[i].catid;
            var data_first_name = trimData(items[i].first_name);
            var data_last_name = trimData(items[i].last_name);
            var data_middle_name = trimData(items[i].middle_name);
            var data_verified_at = trimData(items[i].verified_at);
            var data_clientname = trimData(items[i].clientname);
            var data_categname = trimData(items[i].categname);
            var data_ticket_earn = trimData(items[i].ticket_earn);

            var data_AreasCode = trimData(items[i].areasCode);

            var data_ClientCode = trimData(items[i].clientCode);
            var data_CategoryCode = trimData(items[i].categCode);

            var data_created_at = new Date(items[i].created_at);
            var options = {
                year: "numeric",
                month: "long",
                day: "numeric",
            };
            var formatted_created_at = data_created_at.toLocaleString(
                "en-US",
                options
            );
            var btnView =
                `<i class="fas fa-eye" title="View Record" aria-hidden="true" onclick="TICKETEARNSfindById(` +
                data_id +
                `, true)"></i>`;
            var btnEdit =
                ` / <i class="fas fa-edit" title="Edit Record" aria-hidden="true" onclick="TICKETEARNSfindById(` +
                data_id +
                `, false)"></i>`;
            var btnDel =
                `  <i class="fas fa-trash" title="Remove Record" aria-hidden="true" onclick="deleteConfirmation(` +
                data_id +
                `,TICKETEARNSDeleteById)"></i>`;
            rows += `<tr>`;
            rows += ` <td>` + counter + `</td>`; // No
            rows += ` <td>` + data_clientname + `</td>`; // Total NEW PRF
            rows +=
                ` <td>` +
                data_last_name +
                " " +
                data_first_name +
                ", " +
                data_middle_name +
                `</td>`; // Name

           
    rows +=  `<td>`
        if (data_verified_at == 1){
            rows += `<span class="badge bg-success">VERIFIED</span>`;
        } else{

            rows += `<span class="badge bg-danger">UNVERIFIED</span>`;
        } 
        
  rows += `</td>`;

                
          
   
            rows += ` <td>` + data_categname + `</td>`; // Total Pending
            rows +=
                ` <td>` +
                data_AreasCode +
                "-" +
                data_ClientCode +
                "-" +
                data_CategoryCode +
                "-" +
                data_ticket_earn +
                `</td>`; // Total Pending


            rows += ` <td>` + formatted_created_at + `</td>`; // Total Pending

           
          
            rows += `</tr>`;

            counter++;
        }
        tblTicketearnCodeVerified.append(rows);

        return new DataTable(tblTicketearnCodeVerified, {
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "lengthMenu": [10, 50,100,500] // Define the entries options
        });
        

        
    
    }

}
/** End Function to populate the table view */



   // Add an event listener to the "Check All" button
   document.getElementById('checkAllButton').addEventListener('click', function() {
    // Get all checkboxes with the name "selectedEmployees[]"
    var checkboxes = document.querySelectorAll('input[name="selectedEmployees[]"]');
    
    // Loop through each checkbox
    checkboxes.forEach(function(checkbox) {
        // Toggle the checked state of the checkbox
        checkbox.checked = !checkbox.checked;
    });
});





$(document).ready(function () {
    // Handle form submission
    $("#dataForm").submit(function (e) {
        e.preventDefault();

        // Get the selected values from the multiselect
        var selectedValues = getSelectedValues();
        // return console.log(selectedValues);
        // Add the selected values to the form data
        $(this).append("<input type='hidden' name='selectedValues' value='" + JSON.stringify(selectedValues) + "' />");

        // Uncomment and adjust the AJAX submission if needed
        // return console.log($(this).serialize());
        $.ajax({
            url: "/TICKETEARN-update-data",
            type: "POST",
            data: $(this).serialize(), // Send form data including selected values
            success: function (response) {
                

                Swal.fire({
                    position: "center",
                    icon: response.remarks,
                    title: response.totalEmployees + " " + response.message,
                    showConfirmButton: false,
                    timer: 1000,
                });

                $("#dataForm")[0].reset();
                setTimeout(function () {
                    location.reload();
                }, 1000);
                TICKETEARNSpopulateTable(true, false);
            showToast(response.remarks, response.message);
            },
            error: function (xhr, status, error) {
                console.error(error);
                // Handle server-side error if needed
            },
        });
    });

    // Function to get selected values from multiselect
    function getSelectedValues() {
        var selectedValues = [];

        // Loop through each checked checkbox with the name "selectedEmployees[]"
        $('input[name="selectedEmployees[]"]:checked').each(function() {
            selectedValues.push($(this).val());
        });

        // Return the array of selected values
        return selectedValues;
    }
});
