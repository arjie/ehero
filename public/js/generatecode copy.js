var tblGenerateCode = $("#tblGenerateCode");
var tblGenerateCodeColumns = $("#tblGenerateCode th");
var tblGenerateCodeContent = $("#tblGenerateCode tbody");
var modalBtnNew = $("#modal-btn-new");

var employee_id = $("[name='employee_id']");
// var employee_id = $("[name='employee_id']");
var inputLastName = $("[name='inputLastName']");
var inputMiddleName = $("[name='inputMiddleName']");
var inputPhoneNo = $("[name='inputPhoneNo']");
var inputPosition = $("[name='inputPosition']");
var inputClientName = $("[name='inputClientName']");

/** Start Populate main Table List */
GENERATECODESpopulateMainTable(true, true);
function GENERATECODESpopulateMainTable(ini = false, showMessage = false) {
    $.ajax({
        type: "POST",
        url: "/GENERATECODE-load-items",
        dataType: "json",
        success: function (response) {
            if (showMessage) {
                showToast(response.remarks, response.message);
            }
            var items = response.data.items;
            if (items.length > 0) {
                if (ini) {
                    // script for initial load only
                }
                return GENERATECODESpopulateTable(items);
            }
            return GENERATECODESpopulateTable(false);
        },
        error: function (xhr, status, error) {
            console.log(xhr);
            return tblGenerateCode.append(
                `<tr>` +
                    `<td colspan="` +
                    tblGenerateCodeColumns.length +
                    `">` +
                    xhr.responseJSON.message +
                    `</td>` +
                    `</tr>`
            );
        },
    });
}
/** End Populate main Table List */
/** Start Function to populate the table view */
function GENERATECODESpopulateTable(items) {
    var rows = "";
    if ($.fn.DataTable.isDataTable("#" + tblGenerateCode.attr("id"))) {
        tblGenerateCode.DataTable().destroy();
    }

    tblGenerateCodeContent.empty();

    if (items) {
        if (items.length > 0) {
            var counter = 1;
            for (var i = 0; i < items.length; i++) {
                var data_id = items[i].catid;
                var data_first_name = trimData(items[i].first_name);
                var data_last_name = trimData(items[i].last_name);
                var data_middle_name = trimData(items[i].middle_name);
                var data_phone_no = trimData(items[i].phone_no);
                var data_clientname = trimData(items[i].clientname);
                var data_categname = trimData(items[i].categname);
                var data_ticket_earn = trimData(items[i].ticket_earn);

                var data_AreasCode = trimData(items[i].areasCode);

                var data_ClientCode = trimData(items[i].clientCode);
                var data_CategoryCode = trimData(items[i].categCode);

                var btnView =
                    `<i class="fas fa-eye" title="View Record" aria-hidden="true" onclick="GENERATECODESfindById(` +
                    data_id +
                    `, true)"></i>`;
                var btnEdit =
                    ` / <i class="fas fa-edit" title="Edit Record" aria-hidden="true" onclick="GENERATECODESfindById(` +
                    data_id +
                    `, false)"></i>`;
                var btnDel =
                    `  <i class="fas fa-trash" title="Remove Record" aria-hidden="true" onclick="deleteConfirmation(` +
                    data_id +
                    `,GENERATECODESDeleteById)"></i>`;
                rows += `<tr>`;
                rows += ` <td>` + counter + `</td>`; // No
                rows +=
                    ` <td>` +
                    data_last_name +
                    " " +
                    data_first_name +
                    ", " +
                    data_middle_name +
                    `</td>`; // Name
                rows += ` <td>` + data_phone_no + `</td>`; // Total PRF
                rows += ` <td>` + data_clientname + `</td>`; // Total NEW PRF
                rows += ` <td>` + data_categname + `</td>`; // Total Pending
                rows +=
                    ` <td>` +
                    data_AreasCode +
                    "-" +
                    data_ClientCode +
                    "-" +
                    data_CategoryCode +
                    "-" +
                    data_ticket_earn +
                    `</td>`; // Total Pending
                rows += ` <td>` + btnDel + `</td>`; // Actions
                rows += `</tr>`;
                counter++;
            }

            tblGenerateCode.append(rows);
            return new DataTable("#" + tblGenerateCode.attr("id"));
        }
    }
    return tblGenerateCode.append(
        `<td colspan="` +
            tblGenerateCodeColumns.length +
            `">No data available...</td>`
    );
}
/** End Function to populate the table view */

/** Start Find by ID */
function GENERATECODESfindById(id, isReadOnly = false) {
    $.ajax({
        type: "POST",
        url: "/GENERATECODE-load-item",
        data: {
            employee_id: id,
        },
        dataType: "json",
        success: function (response) {
            showToast(response.remarks, response.message);
            var item = response.data.item;
            if (item.length > 0) {
                employee_id.val(item[0].id);
                employee_id.val(item[0].first_name);
                inputLastName.val(item[0].last_name);
                inputMiddleName.val(item[0].middle_name);
                inputPhoneNo.val(item[0].phone_no);
                inputPosition.val(item[0].position);
                inputClientName.val(item[0].id);

                $("#modal-new").modal("show");
                console.log(item);
            }
        },
        error: function (xhr, status, error) {
            console.log(xhr);
            return showToast("error", "Internal Error!");
        },
    });
}
/** End Find by ID */

/** Start Delete by ID */
function GENERATECODESDeleteById(id) {
    $.ajax({
        type: "POST",
        url: "/GENERATECODE-delete-data",
        data: {
            employee_id: id,
        },
        dataType: "json",
        success: function (response) {
            GENERATECODESpopulateMainTable(false, false);
            showToast(response.remarks, response.message);
        },
        error: function (xhr, status, error) {
            console.log(xhr);
            return showToast("error", "Internal Error!");
        },
    });
}
/** End Delete by ID */

modalBtnNew.click(function (e) {
    employee_id.val(0);
    employee_id.val("");
    inputLastName.val("");
    inputMiddleName.val("");
    inputPhoneNo.val("");
    inputPosition.val("");
    // inputClientName.val("");
    $("#inputClientName option:disabled").prop("selected", true);
    $("#modal-new").modal("show");
});

/*
$(document).ready(function () {
    // Ajax request to fetch client data
    $.ajax({
        url: "/CLIENT-load-mindata", // Replace with the actual URL of your server-side script
        type: "POST",
        dataType: "json",
        success: function (data) {
            // Update the <select> element with the received data
            var select = $("#inputClientName");
            $.each(data.data.items, function (index, items) {
                select.append(
                    $("<option>", {
                        value: items.id,
                        text: items.name,
                    })
                );
            });
        },
        error: function (xhr, status, error) {
            console.error("Error fetching clients:", error);
        },
    });
});
*/
$(document).ready(function () {
    $.ajax({
        url: "/CLIENT-load-mindata",
        type: "POST",
        dataType: "json",
        success: function (data) {
            var select = $("#inputClientName");

            // Clear existing options
            select.empty();

            // Add default option or prompt if needed
            select.append("<option value=''>Select a Client</option>");

            // Populate dropdown with client data
            $.each(data.data.items, function (index, item) {
                select.append(
                    $("<option>", {
                        value: item.id,
                        text: item.name,
                    })
                );
            });
        },
        error: function (xhr, status, error) {
            console.error("Error fetching clients:", error);
        },
    });

    // Add change event listener
    $("#inputClientName").on("change", function () {
        var selectedClientId = $(this).val();
        var employeeList = $("#employee_id");
        employeeList.append(
            $("<option disabled>", {
                value: 0,
                text: "Select Employee",
            })
        );

        employeeList.empty();

        // Perform another AJAX request to get employees based on the selected client ID
        $.ajax({
            url: "/EMPLOYEE-load-mindata",
            type: "POST",
            dataType: "json",
            data: { clientId: selectedClientId },
            success: function (employeeData) {
                // Handle the received employee data
                if (employeeData.remarks === "success") {
                    console.log(employeeData);
                    // Clear existing options

                    // Populate dropdown with employee data
                    $.each(employeeData.data.items, function (index, employee) {
                        employeeList.append(
                            $("<option>", {
                                value: employee.id,
                                text:
                                    employee.first_name +
                                    " " +
                                    employee.last_name,
                            })
                        );
                    });
                } else {
                    employeeList.append(
                        $("<option disabled>", {
                            value: 0,
                            text: "Select Employee",
                        })
                    );
                }
            },
            error: function (xhr, status, error) {
                console.error("Error fetching employees:", error);
            },
        });
    });
});

$("#formNewClient").submit(function (e) {
    e.preventDefault(e);
    var formData = $(this).serializeArray();
    formData.push({ name: "inputClientName", value: inputClientName.val() });

    if (formValidation(formData)) {
        $.ajax({
            type: "POST",
            url: "/GENERATECODE-save-data",
            dataType: "json",
            data: formData,
            success: function (response) {
                if (response.remarks == "success") {
                    GENERATECODESpopulateMainTable(false, false);
                    $("#modal-new").modal("hide");
                } else if (response.remarks == "warning") {
                    //duplicate caught
                    var inputErr = response.data.inputErr;
                    if (inputErr.length > 0) {
                        for (var i = 0; i < inputErr.length; i++) {
                            var inputField = $("[name='" + inputErr[i] + "']");
                            inputField.addClass("is-invalid");
                            inputField.after(
                                `<span class="text-danger">Already taken.</span>`
                            );
                        }
                    }
                }
                return showToast(response.remarks, response.message);
            },
            error: function (xhr, status, error) {
                console.log(xhr);
                return showToast("error", "Internal Error!");
            },
        });
    }
});

$(document).ready(function () {
    // Fetch data using AJAX
    $.ajax({
        url: "/CATEGORY-load-mindata",
        type: "GET",
        dataType: "json",
        success: function (data) {
            if (data.remarks === "success") {
                // Populate form fields dynamically
                var formContainer = $("#formContainer");
                var items = data.data.items;

                for (var i = 0; i < items.length; i++) {
                    if (i % 5 === 0) {
                        // Start a new row after every 5 columns
                        // formContainer.append('<div class="form-group">');
                    }

                    formContainer.append(
                        `<div class="col-md-3">` +
                            '<div class="form-group">' +
                            '<label for="' +
                            items[i].generate_code +
                            '">' +
                            items[i].name +
                            '<span class="text-danger">*</span></label>' +
                            '<input type="number" class="form-control" name="categ_val[]" id="' +
                            items[i].generator_code +
                            '" value="' +
                            items[i].equivalent_no +
                            '" required>' +
                            '<input type="hidden" class="form-control" name="categ_id[]" value="' +
                            items[i].id +
                            '">' +
                            "</div>" +
                            "</div>"
                    );

                    if ((i + 1) % 5 === 0 || i === items.length - 1) {
                        // Close the row after every 5 columns or at the end
                        // formContainer.append("</div>");
                    }
                }
            } else {
                // Handle error case
                console.error(data.message);
                // You may want to display an error message on the page
            }
        },
        error: function (xhr, status, error) {
            // Handle AJAX error
            console.error(error);
            // You may want to display an error message on the page
        },
    });

    // Handle form submission
    $("#dataForm").submit(function (e) {
        e.preventDefault();
        // Uncomment and adjust the AJAX submission if needed
        // return console.log($(this).serialize());
        $.ajax({
            url: "/GENERATECODE-save-data",
            type: "POST",
            data: $(this).serialize(),
            success: function (response) {
                //console.log(response);

                $("#dataForm")[0].reset();
                GENERATECODESpopulateMainTable(false, false);

                Swal.fire({
                    position: "center",
                    icon: response.remarks,
                    title: response.totalTickets + " " + response.message,
                    showConfirmButton: false,
                    timer: 5000,
                });

                // Display SweetAlert with the total number of tickets submitted
            },
            error: function (xhr, status, error) {
                console.error(error);
                // Handle server-side error if needed
            },
        });
    });
});
