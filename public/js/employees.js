var tblEmployees = $("#tblEmployees");
var tblEmployeesColumns = $("#tblEmployees th");
var tblEmployeesContent = $("#tblEmployees tbody");
var modalBtnNew = $("#modal-btn-new");

var employee_id = $("[name='employee_id']");
var inputFirstName = $("[name='inputFirstName']");
var inputLastName = $("[name='inputLastName']");
var inputMiddleName = $("[name='inputMiddleName']");
var inputPhoneNo = $("[name='inputPhoneNo']");
var inputPosition = $("[name='inputPosition']");
var inputClientName = $("[name='inputClientName']");
var inputAreasName = $("[name='inputAreasName']");

/** Start Populate main Table List */
EMPLOYEEpopulateMainTable(true, true);
function EMPLOYEEpopulateMainTable(ini = false, showMessage = false) {
    $.ajax({
        type: "POST",
        url: "/EMPLOYEE-load-items",
        dataType: "json",
        success: function (response) {
            if (showMessage) {
                showToast(response.remarks, response.message);
            }
            var items = response.data.items;
            if (items.length > 0) {
                if (ini) {
                    // script for initial load only
                }
                return EMPLOYEEpopulateTable(items);
            }
            return EMPLOYEEpopulateTable(false);
        },
        error: function (xhr, status, error) {
            console.log(xhr);
            return tblEmployees.append(
                `<tr>` +
                    `<td colspan="` +
                    tblEmployeesColumns.length +
                    `">` +
                    xhr.responseJSON.message +
                    `</td>` +
                    `</tr>`
            );
        },
    });
}
/** End Populate main Table List */
/** Start Function to populate the table view */
function EMPLOYEEpopulateTable(items) {
    var rows = "";
    if ($.fn.DataTable.isDataTable("#" + tblEmployees.attr("id"))) {
        tblEmployees.DataTable().destroy();
    }

    tblEmployeesContent.empty();

    if (items) {
        if (items.length > 0) {
            var counter = 1;
            for (var i = 0; i < items.length; i++) {
                var data_id = items[i].id;
                var data_first_name = trimData(items[i].first_name);
                var data_last_name = trimData(items[i].last_name);
                var data_middle_name = trimData(items[i].middle_name);
                var data_phone_no = trimData(items[i].phone_no);
                var data_position = trimData(items[i].position);
                var data_client_name = trimData(items[i].client_name);
                var data_areasname = trimData(items[i].areas_name);
                var data_totalTicketEarn = trimData(items[i].totalTicketEarn);

                var btnView =
                    `<i class="fas fa-eye" title="View Record" aria-hidden="true" onclick="EMPLOYEEfindById(` +
                    data_id +
                    `, true)"></i>`;
                var btnEdit =
                    ` / <i class="fas fa-edit" title="Edit Record" aria-hidden="true" onclick="EMPLOYEEfindById(` +
                    data_id +
                    `, false)"></i>`;
                var btnDel =
                    ` / <i class="fas fa-trash" title="Remove Record" aria-hidden="true" onclick="deleteConfirmation(` +
                    data_id +
                    `,EMPLOYEEDeleteById)"></i>`;
                rows += `<tr>`;
                rows += ` <td>` + counter + `</td>`; // No
                rows +=
                    ` <td>` +
                    data_last_name +
                    " " +
                    data_first_name +
                    ", " +
                    data_middle_name +
                    `</td>`; // Name
                rows += ` <td>` + data_totalTicketEarn + `</td>`; // Total PRF
                rows += ` <td>` + data_phone_no + `</td>`; // Total PRF
                rows += ` <td>` + data_position + `</td>`; // Total NEW PRF
                rows += ` <td>` + data_client_name + `</td>`; // Total Pending
                rows += ` <td>` + data_areasname + `</td>`; // Total Pending
                rows += ` <td>` + btnView + btnEdit + btnDel + `</td>`; // Actions
                rows += `</tr>`;
                counter++;
            }

            tblEmployees.append(rows);
            return new DataTable("#" + tblEmployees.attr("id"));
        }
    }
    return tblEmployees.append(
        `<td colspan="` +
            tblEmployeesColumns.length +
            `">No data available...</td>`
    );
}
/** End Function to populate the table view */

/** Start Find by ID */
function EMPLOYEEfindById(id, isReadOnly = false) {
    $.ajax({
        type: "POST",
        url: "/EMPLOYEE-load-item",
        data: {
            employee_id: id,
        },
        dataType: "json",
        success: function (response) {
            showToast(response.remarks, response.message);
            var item = response.data.item;
            if (item.length > 0) {
                employee_id.val(item[0].id);
                inputFirstName.val(item[0].first_name);
                inputLastName.val(item[0].last_name);
                inputMiddleName.val(item[0].middle_name);
                inputPhoneNo.val(item[0].phone_no);
                inputPosition.val(item[0].position);
                inputClientName.val(item[0].clientname);
                inputAreasName.val(item[0].areasId);

                $("#modal-new").modal("show");
                console.log(item);
            }
        },
        error: function (xhr, status, error) {
            console.log(xhr);
            return showToast("error", "Internal Error!");
        },
    });
}
/** End Find by ID */

/** Start Delete by ID */
function EMPLOYEEDeleteById(id) {
    $.ajax({
        type: "POST",
        url: "/EMPLOYEE-delete-data",
        data: {
            employee_id: id,
        },
        dataType: "json",
        success: function (response) {
            EMPLOYEEpopulateMainTable(false, false);
            showToast(response.remarks, response.message);
        },
        error: function (xhr, status, error) {
            console.log(xhr);
            return showToast("error", "Internal Error!");
        },
    });
}
/** End Delete by ID */

modalBtnNew.click(function (e) {
    employee_id.val(0);
    inputFirstName.val("");
    inputLastName.val("");
    inputMiddleName.val("");
    inputPhoneNo.val("");
    inputPosition.val("");
    // inputClientName.val("");
    $("#inputClientName option:disabled").prop("selected", true);
    $("#inputAreasName option:disabled").prop("selected", true);
    $("#modal-new").modal("show");
});

$(document).ready(function () {
    // Ajax request to fetch client data
    $.ajax({
        url: "/CLIENT-load-mindata", // Replace with the actual URL of your server-side script
        type: "POST",
        dataType: "json",
        success: function (data) {
            // Update the <select> element with the received data
            var select = $("#inputClientName");
            $.each(data.data.items, function (index, items) {
                select.append(
                    $("<option>", {
                        value: items.id,
                        text: items.name,
                    })
                );
            });
        },
        error: function (xhr, status, error) {
            console.error("Error fetching clients:", error);
        },
    });
});

$(document).ready(function () {
    // Ajax request to fetch client data
    $.ajax({
        url: "/AREAS-load-mindata", // Replace with the actual URL of your server-side script
        type: "POST",
        dataType: "json",
        success: function (data) {
            // Update the <select> element with the received data
            var select = $("#inputAreasName");
            $.each(data.data.items, function (index, items) {
                select.append(
                    $("<option>", {
                        value: items.id,
                        text: items.name,
                    })
                );
            });
        },
        error: function (xhr, status, error) {
            console.error("Error fetching clients:", error);
        },
    });
});

$("#formNewClient").submit(function (e) {
    e.preventDefault(e);
    var formData = $(this).serializeArray();
    formData.push({ name: "inputClientName", value: inputClientName.val() });

    if (formValidation(formData)) {
        $.ajax({
            type: "POST",
            url: "/EMPLOYEE-save-data",
            dataType: "json",
            data: formData,
            success: function (response) {
                if (response.remarks == "success") {
                    EMPLOYEEpopulateMainTable(false, false);
                    $("#modal-new").modal("hide");
                } else if (response.remarks == "warning") {
                    //duplicate caught
                    var inputErr = response.data.inputErr;
                    if (inputErr.length > 0) {
                        for (var i = 0; i < inputErr.length; i++) {
                            var inputField = $("[name='" + inputErr[i] + "']");
                            inputField.addClass("is-invalid");
                            inputField.after(
                                `<span class="text-danger">Already taken.</span>`
                            );
                        }
                    }
                }
                return showToast(response.remarks, response.message);
            },
            error: function (xhr, status, error) {
                console.log(xhr);
                return showToast("error", "Internal Error!");
            },
        });
    }
});
