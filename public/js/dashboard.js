/* global Chart:false */

$(function () {
    "use strict";

    var ticksStyle = {
        fontColor: "#495057",
        fontStyle: "bold",
    };

    var mode = "index";
    var intersect = true;

    var $salesChart = $("#sales-chart");
    // eslint-disable-next-line no-unused-vars
    var salesChart = new Chart($salesChart, {
        type: "bar",
        data: {
            labels: ["JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"],
            datasets: [
                {
                    backgroundColor: "#007bff",
                    borderColor: "#007bff",
                    data: [1000, 2000, 3000, 2500, 2700, 2500, 3000],
                },
                {
                    backgroundColor: "#ced4da",
                    borderColor: "#ced4da",
                    data: [700, 1700, 2700, 2000, 1800, 1500, 2000],
                },
            ],
        },
        options: {
            maintainAspectRatio: false,
            tooltips: {
                mode: mode,
                intersect: intersect,
            },
            hover: {
                mode: mode,
                intersect: intersect,
            },
            legend: {
                display: false,
            },
            scales: {
                yAxes: [
                    {
                        // display: false,
                        gridLines: {
                            display: true,
                            lineWidth: "4px",
                            color: "rgba(0, 0, 0, .2)",
                            zeroLineColor: "transparent",
                        },
                        ticks: $.extend(
                            {
                                beginAtZero: true,

                                // Include a dollar sign in the ticks
                                callback: function (value) {
                                    if (value >= 1000) {
                                        value /= 1000;
                                        value += "k";
                                    }

                                    return "$" + value;
                                },
                            },
                            ticksStyle
                        ),
                    },
                ],
                xAxes: [
                    {
                        display: true,
                        gridLines: {
                            display: false,
                        },
                        ticks: ticksStyle,
                    },
                ],
            },
        },
    });

    var $visitorsChart = $("#visitors-chart");
    // eslint-disable-next-line no-unused-vars
    var visitorsChart = new Chart($visitorsChart, {
        data: {
            labels: ["18th", "20th", "22nd", "24th", "26th", "28th", "30th"],
            datasets: [
                {
                    type: "line",
                    data: [100, 120, 170, 167, 180, 177, 160],
                    backgroundColor: "transparent",
                    borderColor: "#007bff",
                    pointBorderColor: "#007bff",
                    pointBackgroundColor: "#007bff",
                    fill: false,
                    // pointHoverBackgroundColor: '#007bff',
                    // pointHoverBorderColor    : '#007bff'
                },
                {
                    type: "line",
                    data: [60, 80, 70, 67, 80, 77, 100],
                    backgroundColor: "tansparent",
                    borderColor: "#ced4da",
                    pointBorderColor: "#ced4da",
                    pointBackgroundColor: "#ced4da",
                    fill: false,
                    // pointHoverBackgroundColor: '#ced4da',
                    // pointHoverBorderColor    : '#ced4da'
                },
            ],
        },
        options: {
            maintainAspectRatio: false,
            tooltips: {
                mode: mode,
                intersect: intersect,
            },
            hover: {
                mode: mode,
                intersect: intersect,
            },
            legend: {
                display: false,
            },
            scales: {
                yAxes: [
                    {
                        // display: false,
                        gridLines: {
                            display: true,
                            lineWidth: "4px",
                            color: "rgba(0, 0, 0, .2)",
                            zeroLineColor: "transparent",
                        },
                        ticks: $.extend(
                            {
                                beginAtZero: true,
                                suggestedMax: 200,
                            },
                            ticksStyle
                        ),
                    },
                ],
                xAxes: [
                    {
                        display: true,
                        gridLines: {
                            display: false,
                        },
                        ticks: ticksStyle,
                    },
                ],
            },
        },
    });
});

$(document).ready(function () {
    // Ajax request to fetch client data
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });
    $.ajax({
        url: "/CLIENT-load-mindataDash", // Replace with the actual URL of your server-side script
        type: "POST",
        dataType: "json",
        success: function (data) {
            // Update the <select> element with the received data
            var select = $("#Allclient");
            $.each(data.data.items, function (index, item) {
                // Concatenate the HTML string properly
                var html =
                    `<div class="col-12 col-sm-6 col-md-3">` +
                    '<div class="info-box" >' +
                    '<span class="info-box-icon bg-info elevation-1"><i class="fas fa-store"></i></span>' +
                    '<div class="info-box-content">' +
                    '<span class="info-box-text">' +
                    item.name +
                    "</span>" +
                    '<span class="info-box-number" id="' +
                    item.name +
                    '">' +
                    item.TotalEmployee +
                    "</span>" +
                    "</div>" +
                    "</div>" +
                    `</div>`;
                select.append(html);
            });
        },
        error: function (xhr, status, error) {
            console.error("Error fetching clients:", error);
        },
    });
});

// Assuming this JavaScript code is part of your larger script
$.ajax({
    url: "/TicketEarn-load-Top",
    type: "POST",
    dataType: "json",
    success: function (data) {
        // Update the <tbody> element with the received data
        var tbody = $("#listTopEmpTicketEarn");
        $.each(data.data.items, function (index, item) {
            // Concatenate the HTML string properly
            var html =
                "<tr>" +
                "<td>" +
                item.last_name +
                " " +
                item.first_name +
                "</td>" +
                "<td>(" +
                item.name +
                ")</td>" +
                "<td>" +
                item.total_ticket_earn +
                "</td>" +
                "</tr>";
            tbody.append(html);
        });
    },
    error: function (xhr, status, error) {
        console.error("Error fetching clients:", error);
    },
});

$.ajax({
    type: "POST",
    url: "/TicketEarn-load",
    dataType: "json",
    success: function (response) {
        // Changed variable name to response
        // console.log(response);
        if (response.remarks == "success") {
            // Sample data for the bar chart
            var items = response.data.items;
            var labels = [];
            var chartData = []; // Changed variable name to chartData
            for (var i = 0; i < items.length; i++) {
                labels.push(items[i].last_name + " " + items[i].first_name);
                chartData.push(items[i].total_ticket_earn);
            }
            //console.log(chartData);
            drawPieChart("myPieChart3", labels, chartData); // Changed variable name to chartData
        }
    },
    error: function (xhr, txt, err) {
        // $("#total_employees").text("");
        console.log(err);
    },
});

function drawPieChart(elementId, labels, data) {
    var data = {
        labels: labels,
        datasets: [
            {
                data: data,
                backgroundColor: [
                    "rgba(255, 99, 132, 0.7)",
                    "rgba(54, 162, 235, 0.7)",
                    "rgba(255, 206, 86, 0.7)",
                    "rgba(75, 192, 192, 0.7)",
                    "rgba(153, 102, 255, 0.7)",
                ],
            },
        ],
    };
    var options = {
        responsive: true,
        maintainAspectRatio: false,
    };

    ctx = document.getElementById(elementId).getContext("2d");
    myPieChart = new Chart(ctx, {
        type: "pie",
        data: data,
        options: options,
    });
}
