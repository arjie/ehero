var tblPositions = $("#tblPositions");
var tblPositionsColumns = $("#tblPositions th");
var tblPositionsContent = $("#tblPositions tbody");
var modalBtnNew = $("#modal-btn-new");

var position_id = $("[name='position_id']");
var inputClientCode = $("[name='inputClientCode']");
var inputClientName = $("[name='inputClientName']");
var inputClientShortName = $("[name='inputClientShortName']");

/** Start Populate main Table List */
POSITIONSpopulateMainTable(true, true);
function POSITIONSpopulateMainTable(ini = false, showMessage = false) {
    $.ajax({
        type: "POST",
        url: "/POSITION-load-items",
        dataType: "json",
        success: function (response) {
            if (showMessage) {
                showToast(response.remarks, response.message);
            }
            var items = response.data.items;
            if (items.length > 0) {
                if (ini) {
                    // script for initial load only
                }
                return POSITIONSpopulateTable(items);
            }
            return POSITIONSpopulateTable(false);
        },
        error: function (xhr, status, error) {
            console.log(xhr);
            return tblPositions.append(
                `<tr>` +
                    `<td colspan="` +
                    tblPositionsColumns.length +
                    `">` +
                    xhr.responseJSON.message +
                    `</td>` +
                    `</tr>`
            );
        },
    });
}
/** End Populate main Table List */
/** Start Function to populate the table view */
function POSITIONSpopulateTable(items) {
    var rows = "";
    if ($.fn.DataTable.isDataTable("#" + tblPositions.attr("id"))) {
        tblPositions.DataTable().destroy();
    }

    tblPositionsContent.empty();

    if (items) {
        if (items.length > 0) {
            var counter = 1;
            for (var i = 0; i < items.length; i++) {
                var data_id = items[i].id;
                var data_name = trimData(items[i].name);

                var btnView =
                    `<i class="fas fa-eye" title="View Record" aria-hidden="true" onclick="POSITIONSfindById(` +
                    data_id +
                    `, true)"></i>`;
                var btnEdit =
                    ` / <i class="fas fa-edit" title="Edit Record" aria-hidden="true" onclick="POSITIONSfindById(` +
                    data_id +
                    `, false)"></i>`;
                var btnDel =
                    ` / <i class="fas fa-trash" title="Remove Record" aria-hidden="true" onclick="deleteConfirmation(` +
                    data_id +
                    `,POSITIONSDeleteById)"></i>`;
                rows += `<tr>`;
                rows += ` <td>` + counter + `</td>`; // No
                rows += ` <td>` + data_name + `</td>`; // Name
                rows += ` <td>` + 0 + `</td>`; // Total PRF
                rows += ` <td>` + 0 + `</td>`; // Total NEW PRF
                rows += ` <td>` + 0 + `</td>`; // Total Pending
                rows += ` <td>` + btnView + btnEdit + btnDel + `</td>`; // Actions
                rows += `</tr>`;
                counter++;
            }

            tblPositions.append(rows);
            return new DataTable("#" + tblPositions.attr("id"));
        }
    }
    return tblPositions.append(
        `<td colspan="` +
            tblPositionsColumns.length +
            `">No data available...</td>`
    );
}
/** End Function to populate the table view */

/** Start Find by ID */
function POSITIONSfindById(id, isReadOnly = false) {
    $.ajax({
        type: "POST",
        url: "/POSITION-load-item",
        data: {
            position_id: id,
        },
        dataType: "json",
        success: function (response) {
            showToast(response.remarks, response.message);
            var item = response.data.item;
            if (item) {
                position_id.val(item.id);
                inputClientCode.val(item.code);
                inputClientName.val(item.name);
                inputClientShortName.val(item.short_name);
                $("#modal-new").modal("show");
                console.log(item);
            }
        },
        error: function (xhr, status, error) {
            console.log(xhr);
            return showToast("error", "Internal Error!");
        },
    });
}
/** End Find by ID */

/** Start Delete by ID */
function POSITIONSDeleteById(id) {
    $.ajax({
        type: "POST",
        url: "/POSITION-delete-data",
        data: {
            position_id: id,
        },
        dataType: "json",
        success: function (response) {
            POSITIONSpopulateMainTable(false, false);
            showToast(response.remarks, response.message);
        },
        error: function (xhr, status, error) {
            console.log(xhr);
            return showToast("error", "Internal Error!");
        },
    });
}
/** End Delete by ID */

modalBtnNew.click(function (e) {
    position_id.val(0);
    inputClientCode.val("");
    inputClientName.val("");
    inputClientShortName.val("");
    $("#modal-new").modal("show");
});

$("#formNewClient").submit(function (e) {
    e.preventDefault(e);
    var formData = $(this).serializeArray();

    if (formValidation(formData)) {
        $.ajax({
            type: "POST",
            url: "/POSITION-save-data",
            dataType: "json",
            data: formData,
            success: function (response) {
                if (response.remarks == "success") {
                    POSITIONSpopulateMainTable(false, false);
                } else if (response.remarks == "warning") {
                    //duplicate caught
                    var inputErr = response.data.inputErr;
                    if (inputErr.length > 0) {
                        for (var i = 0; i < inputErr.length; i++) {
                            var inputField = $("[name='" + inputErr[i] + "']");
                            inputField.addClass("is-invalid");
                            inputField.after(
                                `<span class="text-danger">Already taken.</span>`
                            );
                        }
                    }
                }
                return showToast(response.remarks, response.message);
            },
            error: function (xhr, status, error) {
                console.log(xhr);
                return showToast("error", "Internal Error!");
            },
        });
    }
});
