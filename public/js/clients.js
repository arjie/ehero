var tblClients = $("#tblClients");
var tblClientsColumns = $("#tblClients th");
var tblClientsContent = $("#tblClients tbody");
var modalBtnNew = $("#modal-btn-new");

var client_id = $("[name='client_id']");
var inputClientCode = $("[name='inputClientCode']");
var inputClientName = $("[name='inputClientName']");
var inputClientShortName = $("[name='inputClientShortName']");

/** Start Populate main Table List */
CLIENTSpopulateMainTable(true, true);
function CLIENTSpopulateMainTable(ini = false, showMessage = false) {
    $.ajax({
        type: "POST",
        url: "/CLIENT-load-items",
        dataType: "json",
        success: function (response) {
            if (showMessage) {
                showToast(response.remarks, response.message);
            }
            var items = response.data.items;
            if (items.length > 0) {
                if (ini) {
                    // script for initial load only
                }
                return CLIENTSpopulateTable(items);
            }
            return CLIENTSpopulateTable(false);
        },
        error: function (xhr, status, error) {
            console.log(xhr);
            return tblClients.append(
                `<tr>` +
                    `<td colspan="` +
                    tblClientsColumns.length +
                    `">` +
                    xhr.responseJSON.message +
                    `</td>` +
                    `</tr>`
            );
        },
    });
}
/** End Populate main Table List */
/** Start Function to populate the table view */
function CLIENTSpopulateTable(items) {
    var rows = "";
    if ($.fn.DataTable.isDataTable("#" + tblClients.attr("id"))) {
        tblClients.DataTable().destroy();
    }

    tblClientsContent.empty();

    if (items) {
        if (items.length > 0) {
            var counter = 1;
            for (var i = 0; i < items.length; i++) {
                var data_id = items[i].id;
                var data_name = trimData(items[i].name);
                var data_shrt_name = trimData(items[i].short_name);
                data_code = trimData(items[i].code);

                var btnView =
                    `<i class="fas fa-eye" title="View Record" aria-hidden="true" onclick="CLIENTSfindById(` +
                    data_id +
                    `, true)"></i>`;
                var btnEdit =
                    ` / <i class="fas fa-edit" title="Edit Record" aria-hidden="true" onclick="CLIENTSfindById(` +
                    data_id +
                    `, false)"></i>`;
                var btnDel =
                    ` / <i class="fas fa-trash" title="Remove Record" aria-hidden="true" onclick="deleteConfirmation(` +
                    data_id +
                    `,CLIENTSDeleteById)"></i>`;
                rows += `<tr>`;
                rows += ` <td>` + counter + `</td>`; // No
                rows += ` <td>` + data_name + `</td>`; // Name
                rows += ` <td>` + data_shrt_name + `</td>`; // Short name
                rows += ` <td>` + data_code + `</td>`; //Code
                rows += ` <td>` + btnView + btnEdit + btnDel + `</td>`; // Actions
                rows += `</tr>`;
                counter++;
            }

            tblClients.append(rows);
            return new DataTable("#" + tblClients.attr("id"));
        }
    }
    return tblClients.append(
        `<td colspan="` +
            tblClientsColumns.length +
            `">No data available...</td>`
    );
}
/** End Function to populate the table view */

/** Start Find by ID */
function CLIENTSfindById(id, isReadOnly = false) {
    $.ajax({
        type: "POST",
        url: "/CLIENT-load-item",
        data: {
            client_id: id,
        },
        dataType: "json",
        success: function (response) {
            showToast(response.remarks, response.message);
            var item = response.data.item;
            if (item) {
                client_id.val(item.id);
                inputClientCode.val(item.code);
                inputClientName.val(item.name);
                inputClientShortName.val(item.short_name);
                // Show the modal
                $("#modal-new").modal("show");

                console.log(item);
            }
        },
        error: function (xhr, status, error) {
            console.log(xhr);
            return showToast("error", "Internal Error!");
        },
    });
}
/** End Find by ID */

/** Start Delete by ID */
function CLIENTSDeleteById(id) {
    $.ajax({
        type: "POST",
        url: "/CLIENT-delete-data",
        data: {
            client_id: id,
        },
        dataType: "json",
        success: function (response) {
            CLIENTSpopulateMainTable(false, false);
            showToast(response.remarks, response.message);
        },
        error: function (xhr, status, error) {
            console.log(xhr);
            return showToast("error", "Internal Error!");
        },
    });
}
/** End Delete by ID */

modalBtnNew.click(function (e) {
    client_id.val(0);
    inputClientCode.val("");
    inputClientName.val("");
    inputClientShortName.val("");
    $("#modal-new").modal("show");
});

$("#formNewClient").submit(function (e) {
    e.preventDefault(e);
    var formData = $(this).serializeArray();

    if (formValidation(formData)) {
        $.ajax({
            type: "POST",
            url: "/CLIENT-save-data",
            dataType: "json",
            data: formData,
            success: function (response) {
                if (response.remarks == "success") {
                    CLIENTSpopulateMainTable(false, false);
                    $("#modal-new").modal("hide");
                } else if (response.remarks == "warning") {
                    //duplicate caught
                    var inputErr = response.data.inputErr;
                    if (inputErr.length > 0) {
                        for (var i = 0; i < inputErr.length; i++) {
                            var inputField = $("[name='" + inputErr[i] + "']");
                            inputField.addClass("is-invalid");
                            inputField.after(
                                `<span class="text-danger">Already taken.</span>`
                            );
                        }
                    }
                }
                return showToast(response.remarks, response.message);
            },
            error: function (xhr, status, error) {
                console.log(xhr);
                return showToast("error", "Internal Error!");
            },
        });
    }
});
