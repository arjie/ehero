var tblGenerateCode = $("#tblGenerateCode");
var tblGenerateCodeColumns = $("#tblGenerateCode th");
var tblGenerateCodeContent = $("#tblGenerateCode tbody");
var modalBtnNew = $("#modal-btn-new");

var employee_id = $("[name='employee_id']");
// var employee_id = $("[name='employee_id']");
var inputLastName = $("[name='inputLastName']");
var inputMiddleName = $("[name='inputMiddleName']");
var inputPhoneNo = $("[name='inputPhoneNo']");
var inputPosition = $("[name='inputPosition']");
var inputClientName = $("[name='inputClientName']");



$(document).ready(function() {
    // Attach change event listener to date inputs
    $('#selectFilterFromDate, #selectFilterToDate').change(function() {
        // Call the function to populate the main table
        GENERATECODESpopulateMainTable();
    });

/** Start Populate main Table List */
GENERATECODESpopulateMainTable(true, true);
function GENERATECODESpopulateMainTable(ini = false, showMessage = false) {

    var fromDate = $('#selectFilterFromDate').val();

    $.ajax({
        type: "POST",
        url: "/GENERATECODE-load-items",
        dataType: "json",
        data: {
            from_date: fromDate,
        },

        success: function (response) {
            if (showMessage) {
                showToast(response.remarks, response.message);
            }
            var items = response.data.items;
            if (items.length > 0) {
                if (ini) {
                    // script for initial load only
                }
                return GENERATECODESpopulateTable(items);
            }
            return GENERATECODESpopulateTable(false);
        },
        error: function (xhr, status, error) {
            console.log(xhr);
            return tblGenerateCode.append(
                `<tr>` +
                    `<td colspan="` +
                    tblGenerateCodeColumns.length +
                    `">` +
                    xhr.responseJSON.message +
                    `</td>` +
                    `</tr>`
            );
        },
    });
}

});
/** End Populate main Table List */
/** Start Function to populate the table view */
function GENERATECODESpopulateTable(items) {
    var rows = "";
    if ($.fn.DataTable.isDataTable("#" + tblGenerateCode.attr("id"))) {
        tblGenerateCode.DataTable().destroy();
    }

    tblGenerateCodeContent.empty();

    if (items) {
        if (items.length > 0) {
            var counter = 1;
            for (var i = 0; i < items.length; i++) {
                var data_id = items[i].catid;
                var data_first_name = trimData(items[i].first_name);
                var data_last_name = trimData(items[i].last_name);
                var data_middle_name = trimData(items[i].middle_name);
                var data_phone_no = trimData(items[i].phone_no);
                var data_clientname = trimData(items[i].clientname);
                var data_categname = trimData(items[i].categname);
                var data_ticket_earn = trimData(items[i].ticket_earn);
                var data_verified_at = trimData(items[i].verified_at);
                var data_AreasCode = trimData(items[i].areasCode);

                var data_ClientCode = trimData(items[i].clientCode);
                var data_CategoryCode = trimData(items[i].categCode);

                var data_created_at = new Date(items[i].created_at);
                var options = {
                    year: "numeric",
                    month: "long",
                    day: "numeric",
                };
                var formatted_created_at = data_created_at.toLocaleString(
                    "en-US",
                    options
                );
                var btnView =
                    `<i class="fas fa-eye" title="View Record" aria-hidden="true" onclick="GENERATECODESfindById(` +
                    data_id +
                    `, true)"></i>`;
                var btnEdit =
                    ` / <i class="fas fa-edit" title="Edit Record" aria-hidden="true" onclick="GENERATECODESfindById(` +
                    data_id +
                    `, false)"></i>`;
                var btnDel =
                    `  <i class="fas fa-trash" title="Remove Record" aria-hidden="true" onclick="deleteConfirmation(` +
                    data_id +
                    `,GENERATECODESDeleteById)"></i>`;
                rows += `<tr>`;
                rows += ` <td>` + counter + `</td>`; // No
                rows +=
                    ` <td>` +
                    data_last_name +
                    " " +
                    data_first_name +
                    ", " +
                    data_middle_name +
                    `</td>`; // Name
                rows += ` <td>` + data_phone_no + `</td>`; // Total PRF
                rows += ` <td>` + data_clientname + `</td>`; // Total NEW PRF
                rows += ` <td>` + data_categname + `</td>`; // Total Pending
                rows +=
                    ` <td>` +
                    data_AreasCode +
                    "-" +
                    data_ClientCode +
                    "-" +
                    data_CategoryCode +
                    "-" +
                    data_ticket_earn +
                    `</td>`; // Total Pending
                                   
        rows +=  `<td>`
        if (data_verified_at == 1){
            rows += `<span class="badge bg-success">VERIFIED</span>`;
        } else{

            rows += `<span class="badge bg-danger">UNVERIFIED</span>`;
        } 
        
  rows += `</td>`;
                rows += ` <td>` + formatted_created_at + `</td>`; // Total Pending
                rows += ` <td>` + btnDel + `</td>`; // Actions
                rows += `</tr>`;

                counter++;
            }
            tblGenerateCode.append(rows);

            // Initialize DataTable
            return new DataTable(tblGenerateCode, {
                dom: "Bfrtip",
                responsive: true,
                lengthChange: false,
                autoWidth: false,
                buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
            });
        }
    }
   
}

/** End Function to populate the table view */

/** Start Find by ID */
function GENERATECODESfindById(id, isReadOnly = false) {
    $.ajax({
        type: "POST",
        url: "/GENERATECODE-load-item",
        data: {
            employee_id: id,
        },
        dataType: "json",
        success: function (response) {
            showToast(response.remarks, response.message);
            var item = response.data.item;
            if (item.length > 0) {
                employee_id.val(item[0].id);
                employee_id.val(item[0].first_name);
                inputLastName.val(item[0].last_name);
                inputMiddleName.val(item[0].middle_name);
                inputPhoneNo.val(item[0].phone_no);
                inputPosition.val(item[0].position);
                inputClientName.val(item[0].id);

                $("#modal-new").modal("show");
                console.log(item);
            }
        },
        error: function (xhr, status, error) {
            console.log(xhr);
            return showToast("error", "Internal Error!");
        },
    });
}
/** End Find by ID */

/** Start Delete by ID */
function GENERATECODESDeleteById(id) {
    $.ajax({
        type: "POST",
        url: "/GENERATECODE-delete-data",
        data: {
            employee_id: id,
        },
        dataType: "json",
        success: function (response) {
            GENERATECODESpopulateMainTable(false, false);
            showToast(response.remarks, response.message);
        },
        error: function (xhr, status, error) {
            console.log(xhr);
            return showToast("error", "Internal Error!");
        },
    });
}
/** End Delete by ID */

modalBtnNew.click(function (e) {
    employee_id.val(0);
    employee_id.val("");
    inputLastName.val("");
    inputMiddleName.val("");
    inputPhoneNo.val("");
    inputPosition.val("");
    // inputClientName.val("");
    $("#inputClientName option:disabled").prop("selected", true);
    $("#modal-new").modal("show");
});

$(document).ready(function () {
    $.ajax({
        url: "/CATEGORY-load-mindata",
        type: "POST",
        dataType: "json",
        success: function (data) {
            var select = $("#Category");

            // Clear existing options
            select.empty();

            // Add default option or prompt if needed
            select.append("<option value=''>Select a Client</option>");

            // Populate dropdown with client data
            $.each(data.data.items, function (index, item) {
                select.append(
                    $("<option>", {
                        value: item.id,
                        text: "(" + item.equivalent_no + ")" + " " + item.name,
                    })
                );
            });
            // select.multiSelect({
            //     keepOrder: true, // If you want to keep the order of selection
            // });
        },
        error: function (xhr, status, error) {
            console.error("Error fetching clients:", error);
        },
    });
});

$(document).ready(function () {
    $.ajax({
        url: "/CLIENT-load-mindata",
        type: "POST",
        dataType: "json",
        success: function (data) {
            var select = $("#inputClientName");

            // Clear existing options
            select.empty();

            // Add default option or prompt if needed
            select.append("<option value=''>Select a Client</option>");

            // Populate dropdown with client data
            $.each(data.data.items, function (index, item) {
                select.append(
                    $("<option>", {
                        value: item.id,
                        text: item.name,
                    })
                );
            });
        },
        error: function (xhr, status, error) {
            console.error("Error fetching clients:", error);
        },
    });

    //
});

// Rest of your existing code
$("#inputClientName").on("change", function () {
    var selectedClientId = $(this).val();
    var employeeListContainer = $("#formContainer");
    employeeListContainer.empty(); // Clear existing content

    // Perform an AJAX request to get employees based on the selected client ID
    $.ajax({
        url: "/EMPLOYEE-load-mindata",
        type: "POST",
        dataType: "json",
        data: { clientId: selectedClientId },
        success: function (employeeData) {
            // Handle the received employee data
            if (employeeData.remarks === "success") {
                // Store the original employee data for filtering
                var originalEmployeeData = employeeData.data.items;

                // Function to populate container with checkboxes for each employee
                function populateEmployeeList(data) {
                    $.each(data, function (index, employee) {
                        var chckbox =
                            `<label class="form-check-label" for="employee_` +
                            employee.id +
                            `">
                            <input type="checkbox" class="form-check-input checkbox" name="selectedEmployees[]" value="` +
                            employee.id +
                            `">
                            ` +
                            employee.last_name +
                            ", " +
                            employee.first_name +
                            " " +
                            employee.middle_name +
                            `
                        </label>`;
                        employeeListContainer.append(
                            $(
                                '<div class="col-lg-2 col-md-2 col-6 search-data">'
                            ).append(
                                $(
                                    "<div class='custom-control custom-checkbox'>"
                                ).append(chckbox)
                            )
                        );
                    });
                }

                // Initial population
                populateEmployeeList(originalEmployeeData);
            } else {
                // Display a message or handle error case
                console.error(
                    "Error fetching employees:",
                    employeeData.message
                );
            }
        },
        error: function (xhr, status, error) {
            console.error("Error fetching employees:", error);
        },
    });
});

document
    .getElementById("employeeSearch")
    .addEventListener("input", function () {
        var searchText = this.value.toLowerCase();

        // Select all divs containing checkboxes
        var checkboxDiv = document.querySelector("div");

        checkboxDiv.querySelectorAll("label").forEach(function (label) {
            var labelText = label.textContent.toLowerCase();

            // Select all elements with class col-lg-2 col-md-2 col-6
            var elements = document.querySelectorAll(".search-data");

            elements.forEach(function (element) {
                var label = element.querySelector(".form-check-label");
                var labelText = label.textContent.toLowerCase();

                // Check if label text contains the search text
                if (labelText.includes(searchText)) {
                    element.classList.remove("d-none"); // Show the element
                } else {
                    element.classList.add("d-none"); // Hide the element
                }
            });
        });
    });
// Initialize count variable
let count = 0;

// Function to update count when a checkbox is clicked
function updateCount() {
    // Get all checkboxes
    const checkboxes = document.querySelectorAll(".form-check-input");

    // Reset count
    count = 0;

    // Loop through checkboxes and count selected ones
    checkboxes.forEach((checkbox) => {
        if (checkbox.checked) {
            count++;
        }
    });

    // Update count in the HTML
    document.getElementById("totalChecked").innerText = count;
}

// Function to check/uncheck all checkboxes
function checkAll() {
    // Find all checkboxes in the formContainer and toggle their checked state
    $("#formContainer input[type='checkbox']").prop(
        "checked",
        function (i, checked) {
            return !checked;
        }
    );

    // Update the count after checking/unchecking all checkboxes
    updateCount();
}

// Attach the updateCount function to individual checkbox click events
$("#formContainer").on("click", ".form-check-input", updateCount);

// Attach the checkAll function to a button click event
$("#checkAllButton").on("click", checkAll);



function getSelectedValues() {
    var selectElement = $(".ms-selected");
    var selectedValues = [];

    for (var i = 0; i < selectElement.length; i++) {
        var nextElement = selectElement[i].nextElementSibling;
        console.log("Selected Values0: ", nextElement);
        console.log("Selected Values1: ", selectElement[i]);
    }
}

$(document).ready(function () {
    // Handle form submission
    $("#dataForm").submit(function (e) {
        e.preventDefault();

        // Uncomment and adjust the AJAX submission if needed
        getSelectedValues();
        // return console.log($(this).serialize());
        $.ajax({
            url: "/GENERATECODE-save-data",
            type: "POST",
            data: $(this).serialize(),
            success: function (response) {
                console.log(response);

                Swal.fire({
                    position: "center",
                    icon: response.remarks,
                    title: response.totalEmployees + " " + response.message,
                    showConfirmButton: false,
                    timer: 5000,
                });

                $("#dataForm")[0].reset();
                setTimeout(function () {
                    location.reload();
                }, 5000);
                GENERATECODESpopulateMainTable(false, false);
                // Display SweetAlert with the total number of tickets submitted
            },
            error: function (xhr, status, error) {
                console.error(error);
                // Handle server-side error if needed
            },
        });
    });
});
