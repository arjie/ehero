/** Ajax Setup Config */
$.ajaxSetup({
    headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
    },
});
/** End of Ajax Setup Config */

function showToast(icon, title, position = "top-end", timer = 3000) {
    var Toast = Swal.mixin({
        toast: true,
        position: position,
        showConfirmButton: false,
        timer: timer,
    });

    return Toast.fire({
        icon: icon,
        title: title,
    });
}

function formValidation(formData, isTest = false) {
    var isOk = true;
    testValidation = isTest;
    $.each(formData, function (index, field) {
        var input = $('[name="' + field.name + '"]');
        console.log(field.name);
        input.removeClass("is-invalid");
        $(input).next(".text-danger").remove();
        if (
            input.hasClass("required") &&
            (field.value === "" ||
                field.value === undefined ||
                field.value === null)
        ) {
            input.addClass("is-invalid");
            isOk = false;
        }
    });

    if (!isOk) {
        showToast("warning", "Insufficient data.");
    }

    if (testValidation) {
        console.log(formData);
        isOk = false;
        showToast(
            "warning",
            "Sorry for the inconvenience. Please reload and try again later!"
        );
    }
    return isOk;
}

function trimData(data, ret = "") {
    if (data != null) {
        data = data.toString();
        if (data.toLowerCase().trim() == "null") {
            return ret;
        }
        return fixSpNchar(data.trim()); //implement fixSpNchar
    }

    return ret;
}

/** Start Null convertion, default empty */
function convertNull(text, convert = "") {
    if (text == "NULL") {
        text = convert;
    }

    return text;
}
/** End Null convertion, default empty */

function convertDate(data, ret = " ", format = "long") {
    var date = new Date(data);

    // Check if the date is equal to January 1, 1970
    if (date.getTime() === new Date("1970-01-01").getTime()) {
        return ret;
    }

    var formattedDate;

    if (format === "long") {
        formattedDate = date.toLocaleString("en-US", {
            year: "numeric",
            month: "long",
            day: "numeric",
        });
    } else if (format === "short") {
        var month = (date.getMonth() + 1).toString().padStart(2, "0");
        var day = date.getDate().toString().padStart(2, "0");
        var year = date.getFullYear();
        formattedDate = `${month}/${day}/${year}`;
    } else {
        return "Invalid format";
    }

    return formattedDate;
}

function convertInputDate(data) {
    var formattedDate = "";
    if (data != null) {
        formattedDate = data.split(" ");
        formattedDate = formattedDate[0];
    }

    return formattedDate;
}

function trimData(data, ret = "") {
    if (data != null) {
        data = data.toString();
        if (data.toLowerCase().trim() == "null") {
            return ret;
        }
        return fixSpNchar(data.trim()); //implement fixSpNchar
    }

    return ret;
}

// fix character Ñ
function fixSpNchar(str) {
    var fxdStr = "";

    for (var i = 0; i < str.length; i++) {
        if (str.charCodeAt(i) == 0xc3) {
            if (str.charCodeAt(++i) == 0xb1) {
                fxdStr += String.fromCharCode(241);
            } else {
                if (str.charCodeAt(i) == 0x3f) {
                    fxdStr += String.fromCharCode(209);
                }
            }
        } else {
            fxdStr += str[i];
        }
    }
    return fxdStr;
}

function deleteConfirmation(id, deleteFunc) {
    Swal.fire({
        title: "Are you sure?",
        text: "You won't be able to revert this!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes, delete it!",
    }).then((result) => {
        if (result.isConfirmed) {
            deleteFunc(id);
        }
    });
}

$(function () {
    //Initialize Select2 Elements
    $(".select2").select2();
});
