@extends('adminlte::page')

@section('title', 'Employees')

@section('content_header')
<!-- <h1>Clients</h1> -->
@stop

@section('content')
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Employee List</h3>
                        <button type="button" class="btn btn-primary float-right" id="modal-btn-new">
                            New
                        </button>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="tblEmployees" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Full Name</th>
                                        <th>Total Ticket Earn</th>
                                        <th>Phone No. </th>
                                        <th>Position</th>
                                        <th>Client</th>
                                        <th>Areas</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>No.</th>
                                        <th>Full Name</th>
                                        <th>Total Ticket Earn</th>
                                        <th>Phone No. </th>
                                        <th>Position</th>
                                        <th>Client</th>
                                        <th>Areas</th>
                                        <th>Actions</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->

    <div class="modal fade" id="modal-new">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Employees Details</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <!-- form start -->
                <form id="formNewClient">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="inputFirstName">First Name <span class="text-danger">*</span></label>
                            <input type="text" class="form-control required" name="inputFirstName" id="inputFirstName" placeholder="Enter Client Code">
                            <input type="hidden" name="employee_id" value="0">
                        </div>
                        <div class="form-group">
                            <label for="inputLastName">Last Name <span class="text-danger">*</span></label>
                            <input type="text" class="form-control required" name="inputLastName" id="inputLastName" placeholder="Enter Client Name">
                        </div>
                        <div class="form-group">
                            <label for="inputMiddleName">Middle Name</label>
                            <input type="text" class="form-control" name="inputMiddleName" id="inputMiddleName" placeholder="Enter Client Short Name">
                        </div>
                        <div class="form-group">
                            <label for="inputPhoneNo">Phone No<span class="text-danger">*</span></label>
                            <input type="text" class="form-control required" name="inputPhoneNo" id="inputPhoneNo" placeholder="Enter Client Short Name">
                        </div>
                        <div class="form-group">
                            <label for="inputPosition">Position</label>
                            <input type="text" class="form-control" name="inputPosition" id="inputPosition" placeholder="Enter Client Short Name">
                        </div>
                        <div class="form-group">
                            <label for="inputAreasName">Areas<span class="text-danger">*</span></label>
                            <select class="form-control required"  name="inputAreasName"  id= "inputAreasName" required>
                               <option selected disabled>Select Areas</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="inputClientName">Client<span class="text-danger">*</span></label>
                            <select class="form-control required"  name="inputClientName"  id= "inputClientName" required>
                               <option selected disabled>Select Client</option>
                            </select>
                        </div>
                        
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
</section>
<!-- /.content -->
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<!-- <script> console.log('Hi!'); </script> -->
<script src="/js/admin_custom.js?<?= uniqid() ?>"></script>
<!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script> -->

<script src="/js/employees.js?<?= uniqid() ?>"></script>
@stop