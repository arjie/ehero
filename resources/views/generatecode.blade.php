@extends('adminlte::page')

@section('title', 'Generate Code')

@section('content_header')
<!-- <h1>Clients</h1> -->
@stop

@section('content')
<!-- Main content -->


<div class="card">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fas fa-chart-pie mr-1"></i>
            Generate Code
        </h3>
        <div class="card-tools">
            <ul class="nav nav-pills ml-auto">
                <li class="nav-item">
                    <a class="nav-link active" href="#generate-code-list" data-toggle="tab">Generate Code List</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#sales-chart" data-toggle="tab">Generate Code</a>
                </li>
            </ul>
        </div>


    </div><!-- /.card-header -->
    <div class="card-body">
        <div class="tab-content p-0">
            <!-- Morris chart - Sales -->
            <div class="chart tab-pane active" id="generate-code-list" style="position: relative;">


                <section class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="inputFromDate">Select</label>
                                <input type="month" name="from_date" id="selectFilterFromDate" class="form-control"
                                    value="{{ date('Y-m') }}">
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-12">
                                <div class="card">


                                    <div class="row">
                                        <div class="col-md-6">

                                        </div>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table id="tblGenerateCode" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Full Name</th>
                                                        <th>Phone No. </th>
                                                        <th>Client</th>
                                                        <th>Category</th>
                                                        <th>Ticket Earn</th>
                                                        <th>Status </th>
                                                        <th>Date Created</th>
                                                        <th>Actions</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Full Name</th>
                                                        <th>Phone No. </th>
                                                        <th>Client</th>
                                                        <th>Category</th>
                                                        <th>Ticket Earn</th>
                                                        <th>Status </th>
                                                        <th>Date Created</th>
                                                        <th>Actions</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.card -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
            </div>
            <div class="chart tab-pane" id="sales-chart" style="position: relative;">
                <form id="dataForm">
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="inputClientName">Client<span
                                                    class="text-danger">*</span></label>
                                            <select class="form-control required" name="inputClientName"
                                                id="inputClientName" required>
                                                <option selected disabled>Select Client</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="Category">Category <span class="text-danger">*</span></label>
                                            <div class="select2-purple">
                                                <select class="select2" multiple="multiple" name="Category[]"
                                                    id="Category" data-placeholder="Select a State"
                                                    style="width: 100%;">
                                                    <option selected disabled>Select Client</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">

                                    <div class="col">
                                        <div class="form-group">
                                            <label for="">Search <span class="text-danger">*</span></label>
                                            <div class="col-md-6 mb-12">
                                                <input type="text" class="form-control" id="employeeSearch"
                                                    placeholder="Search employees">
                                            </div>
                                            {{-- <select class="form-control required" name="Category" id="Category" required>
                                                        <option selected disabled>Select Client</option>
                                                    </select> --}}
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="">Total checked: </label>
                                            <div class="col-md-6 mb-12">
                                                <span class="text-danger" id="totalChecked">*</span>
                                            </div>


                                        </div>
                                    </div>


                                </div>
                            </div>

                            <div class="card-body">
                                <div class="row" id="formContainer"></div>
                            </div>
                        </div>

                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" id="checkAllButton">Check All</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                </form>
            </div>
        </div>
    </div><!-- /.card-body -->
</div>



</section>
<!-- /.content -->
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<!-- <script> console.log('Hi!'); </script> -->
<script src="/js/admin_custom.js?<?= uniqid() ?>"></script>
<!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script> -->

<script src="/js/generatecode.js?<?= uniqid() ?>"></script>
@stop