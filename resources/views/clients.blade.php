@extends('adminlte::page')

@section('title', 'Clients')

@section('content_header')
<!-- <h1>Clients</h1> -->
@stop


@section('content')
<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Client List</h3>
            <button type="button" class="btn btn-primary float-right" id="modal-btn-new">
              New
            </button>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="table-responsive">
              <table id="tblClients" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>Name</th>
                    <th>Short Name</th>
                    <th>Code</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td colspan="7">Waiting for connection...</td>
                  </tr>
                </tbody>
                <tfoot>
                  <tr>
                    <th>No.</th>
                    <th>Name</th>
                    <th>Short Name</th>
                    <th>Code</th>
                    <th>Actions</th>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container-fluid -->

  <div class="modal fade" id="modal-new">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Client Details</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <!-- form start -->
        <form id="formNewClient">
          <div class="modal-body">
            <div class="form-group">
              <label for="inputClientCode">Client Code <span class="text-danger">*</span></label>
              <input type="number" class="form-control required" name="inputClientCode" id="inputClientCode" placeholder="Enter Client Code">
              <input type="hidden" name="client_id" value="0">
            </div>
            <div class="form-group">
              <label for="inputClientName">Client Name <span class="text-danger">*</span></label>
              <input type="text" class="form-control required" name="inputClientName" id="inputClientName" placeholder="Enter Client Name">
            </div>
            <div class="form-group">
              <label for="inputClientShortName">Client Short Name</label>
              <input type="text" class="form-control" name="inputClientShortName" id="inputClientShortName" placeholder="Enter Client Short Name">
            </div>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
</section>
<!-- /.content -->
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<!-- <script> console.log('Hi!'); </script> -->
<script src="/js/admin_custom.js?<?= uniqid() ?>"></script>
<!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script> -->

<script src="/js/clients.js?<?= uniqid() ?>"></script>
@stop