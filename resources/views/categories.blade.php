@extends('adminlte::page')

@section('title', 'Categories')

@section('content_header')
<!-- <h1>Branchs</h1> -->
@stop

@section('content')
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Categories List</h3>
                        <button type="button" class="btn btn-primary float-right" id="modal-btn-new">
                            New
                        </button>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="tblCategories" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Description</th>
                                        <th>Code</th>
                                        <th>Equivalent code</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="8">Waiting for connection...</td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>No.</th>
                                        <th>Description</th>
                                        <th>Code</th>
                                        <th>Equivalent code</th>
                                        <th>Actions</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->

    <div class="modal fade" id="modal-new">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Category Details</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <!-- form start -->  
                <form id="formNewCategory">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="inputCategoryName">Description <span class="text-danger">*</span></label>
                            <input type="text" class="form-control required" name="inputCategoryName" id="inputCategoryName" placeholder="Enter Description Name">
                            <input type="hidden" name="category_id" value="0">
                        </div>
                        <div class="form-group">
                            <label for="inputCategoryCode"> Code <span class="text-danger">*</span></label>
                            <input type="number" class="form-control required" name="inputCategoryCode" id="inputCategoryCode" placeholder="Enter Code">
                        </div>
                        <div class="form-group"> 
                            <label for="inputCategoryEquivalent">Equivalent No.o</label>
                            <input type="number" class="form-control" name="inputCategoryEquivalent" id="inputCategoryEquivalent" placeholder="Enter Equivalent">
                        </div> 
                     </div> 
                     <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                      </div>
                
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
</section>
<!-- /.content -->
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<!-- <script> console.log('Hi!'); </script> -->
<script src="/js/admin_custom.js?<?= uniqid() ?>"></script>

<script src="/js/category.js?<?= uniqid() ?>"></script>
@stop