@extends('adminlte::page')

@section('title', 'Ticket Code')

@section('content_header')
<!-- <h1>Clients</h1> -->
@stop

@section('content')
<!-- Main content -->


<div class="card">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fas fa-chart-pie mr-1"></i>
            Ticket Code
        </h3>
        <div class="card-tools">
            <ul class="nav nav-pills ml-auto">
                <li class="nav-item">
                    <a class="nav-link active" href="#UNVERIFIED" data-toggle="tab">Unverified Ticket</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#VERIFIED" data-toggle="tab">Verified Ticket</a>
                </li>
            </ul>
        </div>


    </div><!-- /.card-header -->
    <div class="card-body">
        <div class="tab-content p-0">
            <!-- Morris chart - Sales -->
            <div class="chart tab-pane active" id="UNVERIFIED" style="position: relative;">


                <section class="content">
                    <div class="container-fluid">


                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <div class="table-responsive">

                                            <div class="row">
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="inputClientName">Client<span
                                                                class="text-danger">*</span></label>
                                                        <select class="form-control required" name="inputClientName"
                                                            id="inputClientName" required>
                                                            <option selected disabled>Select Client</option>
                                                        </select>
                                                    </div>

                                                </div>
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="EmployeeEarns">Employee<span
                                                                class="text-danger">*</span></label>
                                                        <select class="form-control required" name="EmployeeEarns"
                                                            id="EmployeeEarns" required>
                                                            <option selected disabled>Select Employee</option>
                                                        </select>
                                                    </div>

                                                </div>

                                            </div>
                                            <form id="dataForm">
                                                <table id="tblTicketearnCode"
                                                    class="table table-bordered table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>No.</th>
                                                            <th>Client</th>
                                                            <th>Full Name</th>
                                                            <th>Status </th>
                                                            <th>Category</th>
                                                            <th>Ticket Earn</th>
                                                            <th>Date Created</th>
                                                            <th>CheckBox</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th>No.</th>
                                                            <th>Client</th>
                                                            <th>Full Name</th>
                                                            <th>Status </th>
                                                            <th>Category</th>
                                                            <th>Ticket Earn</th>
                                                            <th>Date Created</th>
                                                            <th>CheckBox</th>
                                                        </tr>
                                                    </tfoot>
                                                </table>


                                                <div class="row">
                                                    <div class="col">

                                                        <div class="form-group">
                                                            <label for=""><span class="text-danger">*</span></label>

                                                            <button type="button" class="form-control btn btn-success"
                                                                id="checkAllButton">Check All</button>
                                                        </div>
                                                    </div>
                                                    <div class="col">

                                                        <div class="form-group">
                                                            <label for=""><span class="text-danger">*</span></label>

                                                            <button type="submit"
                                                                class="form-control btn btn-primary">VERIFIED</button>
                                                        </div>
                                                    </div>

                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.card -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
            </div>
            <div class="chart tab-pane" id="VERIFIED" style="position: relative;">
                <form id="dataForm">
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="row">

                                    <div class="table-responsive">

                                        <div class="row">
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="VinputClientName">Client<span
                                                            class="text-danger">*</span></label>
                                                    <select class="form-control required" name="VinputClientName"
                                                        id="VinputClientName" required>
                                                        <option selected disabled>Select Client</option>
                                                    </select>
                                                </div>

                                            </div>
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="VEmployeeEarns">Employee<span
                                                            class="text-danger">*</span></label>
                                                    <select class="form-control required" name="VEmployeeEarns"
                                                        id="VEmployeeEarns" required>
                                                        <option selected disabled>Select Employee</option>
                                                    </select>
                                                </div>

                                            </div>

                                        </div>

                                        <table id="tblTicketearnCodeVerified"
                                            class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>No.</th>
                                                    <th>Client</th>
                                                    <th>Full Name</th>
                                                    <th>Status </th>
                                                    <th>Category</th>
                                                    <th>Ticket Earn</th>
                                                    <th>Date Created</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>No.</th>
                                                    <th>Client</th>
                                                    <th>Full Name</th>
                                                    <th>Status </th>
                                                    <th>Category</th>
                                                    <th>Ticket Earn</th>
                                                    <th>Date Created</th>
                                                </tr>
                                            </tfoot>
                                        </table>



                                    </div>

                                </div>


                            </div>

                        </div>

                        <div class="modal-footer justify-content-between">

                        </div>
                </form>
            </div>
        </div>
    </div><!-- /.card-body -->
</div>



</section>
<!-- /.content -->
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<!-- <script> console.log('Hi!'); </script> -->
<script src="/js/admin_custom.js?<?= uniqid() ?>"></script>
<!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script> -->

<script src="/js/ticketearn.js?<?= uniqid() ?>"></script>
@stop