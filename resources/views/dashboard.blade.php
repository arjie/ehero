@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
<!-- <h1>Welcome</h1> -->
@stop

@section('content')
<!-- Main content -->
<div class="content">
  <div class="container-fluid">




  <div class="row" id="Allclient">
</div>

    <div class="row">
      
      <div class="col-6 d-flex">
        <div class="card w-100">
            <div class="card-header">
                <h3 class="card-title">Top 5 Employees Ticket Earn</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div style="max-width: 1000px; height:500px; margin: auto;">
                    <canvas id="myPieChart3"></canvas>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>

    <div class="col-6 d-flex">
      <div class="card w-100">
          <div class="card-header">
              <h3 class="card-title">Top Employees Ticket Earn</h3>
          </div>
 
          <div class="card-body table-responsive p-0" style="max-height: 500px; overflow-y: auto;">
            <table class="table table-hover text-nowrap">
              <thead>
                <tr>
                  <th>Full Name</th>
                  <th>Client</th>
                  <th>Total Ticket Earn</th>
                </tr>
              </thead>
              <tbody id="listTopEmpTicketEarn">
                <!-- Data will be dynamically populated here using JavaScript -->
              </tbody>
            </table>
          </div>
          


      </div>
      <!-- /.card -->
  </div>

  
    
      <!-- /.col-md-6 -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container-fluid -->
</div>
<!-- /.content -->
@stop

@section('css')
<!-- <link rel="stylesheet" href="/css/admin_custom.css"> -->
@stop

@section('js')
<script src="/js/admin_custom.js?<?= uniqid() ?>"></script>
<script src="/js/dashboard.js?<?= uniqid() ?>"></script>
@stop