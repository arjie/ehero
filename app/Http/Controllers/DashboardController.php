<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index()
    {
        return view('clients');
    }


    public function minDataDash(Request $request)
    {
        $data = [
            "remarks" => 'error',
            "message" => 'Something went wrong',
            "data" => [],
        ];

        $sql = "SELECT clients.id, clients.name, count(employees.client_id) as TotalEmployee
                FROM employees
                INNER JOIN clients ON clients.id = employees.client_id 
                WHERE clients.is_deleted = 0
                GROUP BY clients.id, clients.name
                ORDER BY clients.name;";

        $data["data"]["items"] = DB::select($sql);

        if (!empty($data["data"]["items"])) {
            $data["remarks"] = "success";
            $data["message"] = "Data found successfully!";
        }

        return response()->json($data);
    }


    public function TicketEarn(Request $request)
    {
        $data = [
            "remarks" => 'error',
            "message" => 'Something went wrong',
            "data" => [
                "items" => [],
                "results" => [],
            ],
        ];

        $sql = "SELECT
        employees.id,
        employees.first_name,
        employees.last_name,
        COUNT(ticket_earn.employee_id) AS total_ticket_earn
    FROM employees
    INNER JOIN ticket_earn ON employees.id = ticket_earn.employee_id
    GROUP BY employees.id, employees.first_name, employees.last_name
    ORDER BY total_ticket_earn DESC LIMIT 5";


        $data["data"]["items"] = DB::select($sql);
        if (count($data["data"]["items"]) > 0) {
            $data["remarks"] = "success";
            $data["message"] = "Data successfully found.";
            $data["data"]["results"] = $data["data"]["items"];
        }

        return response()->json($data);
    }


    public function TopTicketEarn(Request $request)
    {
        $data = [
            "remarks" => 'error',
            "message" => 'Something went wrong',
            "data" => [
                "items" => [],
                "results" => [],
            ],
        ];

        $sql = "SELECT
        employees.id,
        employees.first_name,
        employees.last_name,
        COUNT(ticket_earn.employee_id) AS total_ticket_earn,
        employees.client_id,
        clients.name

    FROM employees
    LEFT JOIN ticket_earn ON employees.id = ticket_earn.employee_id
    INNER JOIN clients ON employees.client_id = clients.id
    GROUP BY employees.id, employees.first_name, employees.last_name, employees.client_id, clients.name
    ORDER BY total_ticket_earn DESC";



        $data["data"]["items"] = DB::select($sql);
        if (count($data["data"]["items"]) > 0) {
            $data["remarks"] = "success";
            $data["message"] = "Data successfully found.";
            $data["data"]["results"] = $data["data"]["items"];
        }

        return response()->json($data);
    }



    /*
    public function validateFields($field, $ret = null)
    {
        if (
            $field === 0
            || $field === "0"
        ) {
            return $field;
        }
        return ($field) ? $field : $ret;
    }
    public function minData(Request $request)
    {
        $data = [
            "remarks" => 'error',
            "message" => 'Something went wrong', // Corrected the key here
            "data" => [],
        ];

        $sql  = "SELECT clients.id, clients.name
        FROM clients
        WHERE clients.is_deleted = 0
        ORDER BY clients.name;";


        $data["data"]["items"] = DB::select($sql);

        if (count($data["data"]["items"]) > 0) {
            $data["remarks"] = "success";
            $data["message"] = "Data found successfully!"; // Corrected the key here
        }

        return response()->json($data);
    }




    public function loadItems(Request $request)
    {
        $data = [
            "remarks" => 'error',
            "message" => 'Something went wrong',
            "data" => [],
        ];

        $sql  = "SELECT clients.id, clients.name, clients.short_name, clients.code
        FROM clients
        WHERE clients.is_deleted = 0
        ORDER BY clients.name;";
        $data["data"]["items"] = DB::select($sql);

        if (count($data["data"]["items"]) > 0) {
            $data["remarks"] = "success";
            $data["message"] = "Data found successfully!";
        }

        return response()->json($data);
    }



    public function loadItem(Request $request)
    {
        $data = [
            "remarks" => 'error',
            "message" => 'Something went wrong',
            "data" => [],
        ];

        $id = $request->client_id;

        $result = DB::select("SELECT id, name, short_name, code FROM clients WHERE is_deleted = 0 AND id = ? LIMIT 1", [$id]);

        // Check if a result is found before using it
        if (!empty($result)) {
            $data["data"]["item"] = $result[0];
            $data["remarks"] = "success";
            $data["message"] = "Data found successfully!";
        }

        return response()->json($data);
    }

    public function saveData(Request $request)
    {
        $data = [
            "remarks" => 'error',
            "message" => '',
            "data" => [],
        ];

        $user_id = Auth::id();

        $client_id = $request->client_id;
        $inputClientCode = $request->inputClientCode;
        $inputClientName = $request->inputClientName;
        $inputClientShortName = $request->inputClientShortName;

        DB::beginTransaction();
        try {

            $validateDuplicate = DB::table('clients')
                ->select('name', 'short_name', 'code')
                ->where('id', '<>', $client_id)
                ->where(function ($query) use ($inputClientName, $inputClientShortName, $inputClientCode) {
                    $query->where('name', $inputClientName)
                        ->orWhere('short_name', $inputClientShortName)
                        ->orWhere('code', $inputClientCode);
                })
                ->take(1)
                ->get();

            if (!$validateDuplicate->isEmpty()) {
                $data["remarks"] = "warning";
                $data["message"] = "Data already in database";
                if ($validateDuplicate[0]->name == $inputClientName) {
                    $data["data"]["inputErr"][] = "inputClientName";
                }
                if ($validateDuplicate[0]->short_name == $inputClientShortName) {
                    $data["data"]["inputErr"][] = "inputClientShortName";
                }
                if ($validateDuplicate[0]->code == $inputClientCode) {
                    $data["data"]["inputErr"][] = "inputClientCode";
                }
                return response()->json($data);
            }

            if ($client_id > 0) {
                DB::table('clients')
                    ->where('id', $client_id)
                    ->update([
                        'name' => $inputClientName,
                        'short_name' => $inputClientShortName,
                        'code' => $inputClientCode,
                        'modified_by' => $user_id,
                        'modified_at' => now()
                    ]);
                $data["message"] = "Data successfully saved in database!";
            } else {
                DB::table('clients')->insert([
                    'name' => $inputClientName,
                    'short_name' => $inputClientShortName,
                    'code' => $inputClientCode,
                    'created_by' => $user_id,
                ]);
                $data["message"] = "Data successfully inserted in database!";
            }

            $data["remarks"] = "success";
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            $data["remarks"] = "error";
            $data["message"] = print_r($e);
        }

        return response()->json($data);
    }
    public function deleteData(Request $request)
    {
        $data = [
            "remarks" => 'error',
            "message" => '',
            "data" => [],
        ];

        $user_id = Auth::id();

        $client_id = $request->client_id;

        DB::beginTransaction();
        try {
            DB::table('clients')
                ->where('id', $client_id)
                ->update([
                    'is_deleted' => '1',
                    'modified_by' => $user_id,
                    'modified_at' => now()
                ]);

            $data["remarks"] = "success";
            $data["message"] = "Data successfully tag deleted in database!";
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            $data["remarks"] = "error";
            $data["message"] = print_r($e);
        }

        return response()->json($data);
    }
    */
}
