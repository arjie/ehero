<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PRFsController extends Controller
{
    public function index()
    {
        return view('prfs');
    }

    public function validateFields($field, $ret = null)
    {
        if (
            $field === 0
            || $field === "0"
        ) {
            return $field;
        }
        return ($field) ? $field : $ret;
    }

    public function loadItems(Request $request)
    {
        $data = [
            "remarks" => 'error',
            "message" => 'Something went wrong',
            "data" => [],
        ];

        
        

        $sql  = "SELECT clients.id, clients.name, clients.short_name, clients.code
        FROM clients
        WHERE clients.is_deleted = 0
        ORDER BY clients.name;";
        $data["data"]["items"] = DB::select($sql);

        if (count($data["data"]["items"]) > 0) {
            $data["remarks"] = "success";
            $data["message"] = "Data found successfully!";
        }

        return response()->json($data);
    }

    public function loadItem(Request $request)
    {
        $data = [
            "remarks" => 'error',
            "message" => 'Something went wrong',
            "data" => [],
        ];

        $id = $request->client_id;

        $sql  = "SELECT TOP(1) clients.[id], clients.[name], clients.[short_name], clients.[code]
            FROM clients
            WHERE clients.[id] = $id";
        $data["data"]["item"] = DB::select($sql);

        if (count($data["data"]["item"]) > 0) {
            $data["remarks"] = "success";
            $data["message"] = "Data found successfully!";
        }

        return response()->json($data);
    }

    public function saveData(Request $request)
    {
        $data = [
            "remarks" => 'error',
            "message" => '',
            "data" => [],
        ];

        $user_id = Auth::id();

        $client_id = $request->client_id;
        $inputClientCode = $request->inputClientCode;
        $inputClientName = $request->inputClientName;
        $inputClientShortName = $request->inputClientShortName;

        DB::beginTransaction();
        try {

            $sql = "SELECT TOP(1) [name], [short_name], [code]
                    FROM clients
                    WHERE
                        [id] <> ?
                        AND (
                            [name] = ?
                            OR [short_name] = ?
                            OR [code] = ?
                        )
                    ";
            $validateDuplicate = DB::select($sql, [
                $client_id,
                $inputClientName,
                $inputClientShortName,
                $inputClientCode,
            ]);
            if (count($validateDuplicate) > 0) {
                $data["remarks"] = "warning";
                $data["message"] = "Data already in database";
                if ($validateDuplicate[0]->name == $inputClientName) {
                    $data["data"]["inputErr"][] = "inputClientName";
                }
                if ($validateDuplicate[0]->short_name == $inputClientShortName) {
                    $data["data"]["inputErr"][] = "inputClientShortName";
                }
                if ($validateDuplicate[0]->code == $inputClientCode) {
                    $data["data"]["inputErr"][] = "inputClientCode";
                }
                return response()->json($data);
            }

            if ($client_id > 0) {
                DB::update(
                    'UPDATE clients
                        SET
                            [name] = ?,
                            [short_name] = ?,
                            [code] = ?,
                            [modified_by] = ?,
                            [modified_at] = ?
                        WHERE
                            [id] = ?
                        ',
                    [
                        $inputClientName,
                        $inputClientShortName,
                        $inputClientCode,
                        $user_id,
                        now(),

                        $client_id,
                    ]
                );
                $data["message"] = "Data successfully saved in database!";
            } else {
                DB::insert(
                    'INSERT INTO clients
                            ( [name], [short_name], [code], [created_by] )
                            VALUES
                            ( ?, ?, ?, ?)
                        ',
                    [
                        $inputClientName,
                        $inputClientShortName,
                        $inputClientCode,
                        $user_id,
                    ]
                );
                $data["message"] = "Data successfully inserted in database!";
            }

            $data["remarks"] = "success";
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            $data["remarks"] = "error";
            $data["message"] = print_r($e);
        }

        return response()->json($data);
    }
    public function deleteData(Request $request)
    {
        $data = [
            "remarks" => 'error',
            "message" => '',
            "data" => [],
        ];

        $user_id = Auth::id();

        $client_id = $request->client_id;

        DB::beginTransaction();
        try {
            DB::update(
                'UPDATE clients
                    SET
                        [is_deleted] = ?,
                        [modified_by] = ?,
                        [modified_at] = ?
                    WHERE
                        [id] = ?
                    ',
                [
                    1,
                    $user_id,
                    now(),

                    $client_id,
                ]
            );

            $data["remarks"] = "success";
            $data["message"] = "Data successfully tag deleted in database!";
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            $data["remarks"] = "error";
            $data["message"] = print_r($e);
        }

        return response()->json($data);
    }
}
