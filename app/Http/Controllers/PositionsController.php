<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PositionsController extends Controller
{
    public function index()
    {
        return view('positions');
    }

    public function validateFields($field, $ret = null)
    {
        if (
            $field === 0
            || $field === "0"
        ) {
            return $field;
        }
        return ($field) ? $field : $ret;
    }

    public function loadItems(Request $request)
    {
        $data = [
            "remarks" => 'error',
            "message" => 'Something went wrong',
            "data" => [],
        ];

        $data["data"]["items"] = DB::table('clients')
            ->select('id', 'name', 'short_name', 'code')
            ->where('is_deleted', '0')
            ->get();

        if (count($data["data"]["items"]) > 0) {
            $data["remarks"] = "success";
            $data["message"] = "Data found successfully!";
        }

        return response()->json($data);
    }

    public function loadItem(Request $request)
    {
        $data = [
            "remarks" => 'error',
            "message" => 'Something went wrong',
            "data" => [],
        ];

        $id = $request->client_id;

        $data["data"]["item"] = DB::table('clients')
            ->select('id', 'name', 'short_name', 'code')
            ->where('is_deleted', '0')
            ->where('id', $id)
            ->find(1);

        if ($data["data"]["item"]) {
            $data["remarks"] = "success";
            $data["message"] = "Data found successfully!";
        }

        return response()->json($data);
    }

    public function saveData(Request $request)
    {
        $data = [
            "remarks" => 'error',
            "message" => '',
            "data" => [],
        ];

        $user_id = Auth::id();

        $client_id = $request->client_id;
        $inputClientCode = $request->inputClientCode;
        $inputClientName = $request->inputClientName;
        $inputClientShortName = $request->inputClientShortName;

        DB::beginTransaction();
        try {

            $validateDuplicate = DB::table('clients')
                ->select('name', 'short_name', 'code')
                ->where('id', '<>', $client_id)
                ->where(function ($query) use ($inputClientName, $inputClientShortName, $inputClientCode) {
                    $query->where('name', $inputClientName)
                        ->orWhere('short_name', $inputClientShortName)
                        ->orWhere('code', $inputClientCode);
                })
                ->take(1)
                ->get();

            if (!$validateDuplicate->isEmpty()) {
                $data["remarks"] = "warning";
                $data["message"] = "Data already in database";
                if ($validateDuplicate[0]->name == $inputClientName) {
                    $data["data"]["inputErr"][] = "inputClientName";
                }
                if ($validateDuplicate[0]->short_name == $inputClientShortName) {
                    $data["data"]["inputErr"][] = "inputClientShortName";
                }
                if ($validateDuplicate[0]->code == $inputClientCode) {
                    $data["data"]["inputErr"][] = "inputClientCode";
                }
                return response()->json($data);
            }

            if ($client_id > 0) {
                DB::table('clients')
                    ->where('id', $client_id)
                    ->update([
                        'name' => $inputClientName,
                        'short_name' => $inputClientShortName,
                        'code' => $inputClientCode,
                        'modified_by' => $user_id,
                        'modified_at' => now()
                    ]);
                $data["message"] = "Data successfully saved in database!";
            } else {
                DB::table('clients')->insert([
                    'name' => $inputClientName,
                    'short_name' => $inputClientShortName,
                    'code' => $inputClientCode,
                    'created_by' => $user_id,
                ]);
                $data["message"] = "Data successfully inserted in database!";
            }

            $data["remarks"] = "success";
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            $data["remarks"] = "error";
            $data["message"] = print_r($e);
        }

        return response()->json($data);
    }
    public function deleteData(Request $request)
    {
        $data = [
            "remarks" => 'error',
            "message" => '',
            "data" => [],
        ];

        $user_id = Auth::id();

        $client_id = $request->client_id;

        DB::beginTransaction();
        try {
            DB::table('clients')
                ->where('id', $client_id)
                ->update([
                    'is_deleted' => '1',
                    'modified_by' => $user_id,
                    'modified_at' => now()
                ]);

            $data["remarks"] = "success";
            $data["message"] = "Data successfully tag deleted in database!";
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            $data["remarks"] = "error";
            $data["message"] = print_r($e);
        }

        return response()->json($data);
    }
}
