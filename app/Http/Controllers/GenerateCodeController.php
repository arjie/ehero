<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GenerateCodeController extends Controller
{
    public function index()
    {
        return view('generatecode');
    }

    public function validateFields($field, $ret = null)
    {
        if (
            $field === 0
            || $field === "0"
        ) {
            return $field;
        }
        return ($field) ? $field : $ret;
    }

    public function loadItems(Request $request)
    {


        $fromDate = $request->input('from_date') . '-01';
        $fromDates = $request->input('from_date');
        
        $data = [
            "remarks" => 'error',
            "message" => 'Something went wrong',
            "data" => [],
        ];


if (empty($fromDates)) {

    $sql = "SELECT c.id as catid ,   c.ticket_earn, e.id, e.first_name, e.last_name, e.middle_name, e.phone_no, e.position ,c.created_at ,
    ct.generator_code as categCode,
    ct.name as categname ,
    cl.name as clientname,
    cl.code as clientCode,
    ar.code as areasCode

    FROM ticket_earn c
    JOIN employees e ON e.id  = c.employee_id
    JOIN categories ct ON ct.id  = c.category_id
    JOIN clients cl ON cl.id  = e.client_id
    JOIN areas ar on ar.id = e.areas_id

    WHERE c.is_deleted = 0 
    AND c.verified_at =1
    ORDER BY c.created_at DESC";


$data["data"]["items"] = DB::select($sql);

if (count($data["data"]["items"]) > 0) {
    $data["remarks"] = "success";
    $data["message"] = "Data found successfully!";
}


}else{
    $sql = "SELECT c.id as catid ,   c.ticket_earn, e.id, e.first_name, e.last_name, e.middle_name, e.phone_no, e.position ,c.created_at ,
    ct.generator_code as categCode,
    ct.name as categname ,
    cl.name as clientname,
    cl.code as clientCode,
    ar.code as areasCode

    FROM ticket_earn c
    JOIN employees e ON e.id  = c.employee_id
    JOIN categories ct ON ct.id  = c.category_id
    JOIN clients cl ON cl.id  = e.client_id
    JOIN areas ar on ar.id = e.areas_id

    WHERE c.is_deleted = 0  
    AND c.verified_at =1
    AND MONTH(c.created_at) = MONTH('$fromDate') 
    
    ORDER BY c.created_at DESC";

$data["data"]["items"] = DB::select($sql);

if (count($data["data"]["items"]) > 0) {
    $data["remarks"] = "success";
    $data["message"] = "Data found successfully!";
}


}
   

        return response()->json($data);
    }

    /*public function loadItem(Request $request)
    {
        $data = [
            "remarks" => 'error',
            "message" => 'Something went wrong',
            "data" => [],
        ];

        $id = $request->employee_id;

        $sql  = "SELECT  e.id, e.first_name, e.last_name, e.middle_name, e.phone_no, e.position, c.name
        FROM employees e
        JOIN clients c ON e.client_id = c.id
        WHERE e.id = $id";
        $data["data"]["item"] = DB::select($sql);

        if (count($data["data"]["item"]) > 0) {
            $data["remarks"] = "success";
            $data["message"] = "Data found successfully!";
        }

        return response()->json($data);
    }
    */

    public function saveData(Request $request)
    {
        $data = [
            "remarks" => 'error',
            "message" => '',
            "data" => [],
        ];

        $user_id = Auth::id();
        $inputClientName = $request->inputClientName;
        $employee_ids = $request->selectedEmployees; // Changed variable name to be more accurate
        $transaction_code = date("Ymd") . uniqid();
        $categ_id = $request->Category;

        DB::beginTransaction();
        try {
            $sql  = "SELECT categories.id, categories.equivalent_no
                FROM categories
                WHERE categories.is_deleted = 0
                ORDER BY categories.name;";

            $category_values = DB::select($sql);

            $totalEmployees = count(array_unique($employee_ids)); // Count unique employees

            for ($e = 0; $e < count($employee_ids); $e++) {
                for ($i = 0; $i < count($categ_id); $i++) {
                    $defaultValue = 0;
                    for ($val = 0; $val < count($category_values); $val++) {
                        if ($categ_id[$i] == $category_values[$val]->id) {
                            $defaultValue = intval($category_values[$val]->equivalent_no);
                            break;
                        }
                    }

                    if ($defaultValue > 0) {
                        for ($j = 0; $j < $defaultValue; $j++) {
                            do {
                                $randTicket = $this->generateRandomTicket(8);
                            } while ($this->isTicketDuplicate($randTicket));

                            DB::insert(
                                'INSERT INTO ticket_earn
                                    (
                                        `employee_id`, `category_id`, `ticket_earn`, `transaction_code`, `created_by`
                                    )
                                    VALUES
                                    (?, ?, ?, ?, ?)',
                                [
                                    $employee_ids[$e],
                                    $categ_id[$i],
                                    $randTicket,
                                    $transaction_code,
                                    $user_id
                                ]
                            );
                        }
                    }
                }
            }

            $data["message"] = "Employees were successfully inserted in the database!";
            $data["remarks"] = "success";
            $data["totalEmployees"] = $totalEmployees;

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            $data["remarks"] = "error";
            $data["message"] = $e->getMessage();
        }

        return response()->json($data);
    }


    public function deleteData(Request $request)
    {
        $data = [
            "remarks" => 'error',
            "message" => '',
            "data" => [],
        ];

        $user_id = Auth::id();

        $employee_id = $request->employee_id;

        DB::beginTransaction();
        try {
            DB::update(
                'UPDATE ticket_earn
                    SET
                        is_deleted = ?,
                        modified_by = ?
                    WHERE
                        id = ?
                    ',
                [
                    1,
                    $user_id,

                    $employee_id,
                ]
            );

            $data["remarks"] = "success";
            $data["message"] = "Data successfully tag deleted in database!";
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            $data["remarks"] = "error";
            $data["message"] = print_r($e);
        }

        return response()->json($data);
    }

    public function generateRandomTicket($length)
    {
        //validation of duplicate

        $min = pow(10, $length - 1);
        $max = pow(10, $length) - 1;

        return mt_rand($min, $max);
    }
    private function isTicketDuplicate($ticket)
    {
        // Check if the ticket number already exists in the database
        // Replace 'ticket_earn' with your actual table name
        $existingTicket = DB::table('ticket_earn')->where('ticket_earn', $ticket)->first();

        return !empty($existingTicket);
    }
}
