<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CategoriesController extends Controller
{
    public function index()
    {
        return view('categories'); // bladex default index
    }

    public function validateFields($field, $ret = null)
    {
        if (
            $field === 0
            || $field === "0"
        ) {
            return $field;
        }
        return ($field) ? $field : $ret;
    }

    public function loadItems(Request $request)
    {
        $data = [
            "remarks" => 'error',
            "message" => 'Something went wrong',
            "data" => [],
        ];


        $sql  = "SELECT categories.id, categories.name, categories.generator_code, categories.equivalent_no
        FROM categories
        WHERE categories.is_deleted = 0
        ORDER BY categories.name desc";

        $data["data"]["items"] = DB::select($sql);

        if (count($data["data"]["items"]) > 0) {
            $data["remarks"] = "success";
            $data["message"] = "Data found successfully!";
        }

        return response()->json($data);
    }




    public function loadItem(Request $request)
    {
        $data = [
            "remarks" => 'error',
            "message" => 'Something went wrong',
            "data" => [],
        ];

        $id = $request->category_id;
        $result = DB::select("SELECT id, name, generator_code, equivalent_no FROM categories WHERE is_deleted = 0 AND id = ? LIMIT 1", [$id]);

        // Check if a result is found before using it
        if (!empty($result)) {
            $data["data"]["item"] = $result[0];
            $data["remarks"] = "success";
            $data["message"] = "Data found successfully!";
        }

        return response()->json($data);
    }

    public function saveData(Request $request)
    {
        $data = [
            "remarks" => 'error',
            "message" => '',
            "data" => [],
        ];

        $user_id = Auth::id();

        $category_id = $request->category_id;
        $inputCategoryCode = trim($request->inputCategoryCode);
        $inputCategoryName = trim($request->inputCategoryName);
        $inputCategoryEquivalent = trim($request->inputCategoryEquivalent);

        DB::beginTransaction();
        try {
            $validateDuplicate = DB::table('categories')
                ->select('name', 'generator_code')
                ->where('id', '<>', $category_id)
                ->where(function ($query) use ($inputCategoryName, $inputCategoryCode) {
                    $query->where('name', $inputCategoryName)
                        ->orWhere('generator_code', $inputCategoryCode);
                })
                ->take(1)
                ->get();
            if (count($validateDuplicate) > 0) {
                $data["remarks"] = "warning";
                $data["message"] = "Data already in database";
                if (trim($validateDuplicate[0]->name) == $inputCategoryName) {
                    $data["data"]["inputErr"][] = "inputCategoryName";
                }
                if (trim($validateDuplicate[0]->generator_code) == $inputCategoryCode) {
                    $data["data"]["inputErr"][] = "inputCategoryCode";
                }
                return response()->json($data);
            }

            if ($category_id > 0) {
                DB::table('categories')
                    ->where('id', $category_id)
                    ->update([
                        'name' => $inputCategoryName,
                        'generator_code' => $inputCategoryCode,
                        'equivalent_no' => $inputCategoryEquivalent,
                        'modified_by' => $user_id,
                        'modified_at' => now(),
                    ]);
                $data["message"] = "Data successfully saved in database!";
            } else {
                DB::table('categories')->insert([
                    'name' => $inputCategoryName,
                    'generator_code' => $inputCategoryCode,
                    'equivalent_no' => $inputCategoryEquivalent,
                    'created_by' => $user_id,

                ]);
                $data["message"] = "Data successfully inserted in database!";
            }

            $data["remarks"] = "success";
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            $data["remarks"] = "error";
            $data["message"] = print_r($e);
        }

        return response()->json($data);
    }
    public function deleteData(Request $request)
    {
        $data = [
            "remarks" => 'error',
            "message" => '',
            "data" => [],
        ];

        $user_id = Auth::id();

        $category_id = $request->category_id;

        DB::beginTransaction();
        try {
            DB::table('categories')
                ->where('id', $category_id)
                ->update([
                    'is_deleted' => '1',
                    'modified_by' => $user_id,
                    'modified_at' => now()
                ]);

            $data["remarks"] = "success";
            $data["message"] = "Data successfully tag deleted in database!";
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            $data["remarks"] = "error";
            $data["message"] = print_r($e);
        }

        return response()->json($data);
    }





    public function getCategories()
    {
        $data = [
            "remarks" => 'error',
            "message" => 'Something went wrong', // Corrected the key here
            "data" => [],
        ];

        $sql  = "SELECT *
    FROM categories
    WHERE is_deleted = 0
    ORDER BY id;";


        $data["data"]["items"] = DB::select($sql);

        if (count($data["data"]["items"]) > 0) {
            $data["remarks"] = "success";
            $data["message"] = "Data found successfully!"; // Corrected the key here
        }

        return response()->json($data);
    }
}
