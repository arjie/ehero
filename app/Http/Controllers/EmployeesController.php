<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EmployeesController extends Controller
{
    public function index()
    {
        return view('employees');
    }

    public function validateFields($field, $ret = null)
    {
        if (
            $field === 0
            || $field === "0"
        ) {
            return $field;
        }
        return ($field) ? $field : $ret;
    }

    public function loadItems(Request $request)
    {
        $data = [
            "remarks" => 'error',
            "message" => 'Something went wrong',
            "data" => [],
        ];

        $sql = "SELECT
        e.id,
        e.first_name,
        e.last_name,
        e.middle_name,
        e.phone_no,
        e.position,
        c.name AS client_name,
        d.name AS areas_name,
        COUNT(te.ticket_earn) AS totalTicketEarn
    FROM
        employees e
    JOIN clients c ON e.client_id = c.id
    JOIN areas d ON e.areas_id = d.id
    LEFT JOIN ticket_earn te ON e.id = te.employee_id
    WHERE
        e.is_deleted = 0
    GROUP BY
        e.id, e.first_name, e.last_name, e.middle_name, e.phone_no, e.position, c.name, d.name
    ORDER BY
        e.id";


        $data["data"]["items"] = DB::select($sql);

        if (count($data["data"]["items"]) > 0) {
            $data["remarks"] = "success";
            $data["message"] = "Data found successfully!";
        }

        return response()->json($data);
    }

    public function loadItem(Request $request)
    {
        $data = [
            "remarks" => 'error',
            "message" => 'Something went wrong',
            "data" => [],
        ];

        $id = $request->employee_id;

        $sql  = "SELECT e.id, e.first_name, e.last_name, e.middle_name, e.phone_no, e.position, c.name ,d.name as areasname,d.id as areasId ,c.id as clientname
        FROM employees e
        JOIN clients c ON e.client_id = c.id
 		JOIN areas d ON e.areas_id = d.id
        WHERE e.id = $id";
        $data["data"]["item"] = DB::select($sql);

        if (count($data["data"]["item"]) > 0) {
            $data["remarks"] = "success";
            $data["message"] = "Data found successfully!";
        }

        return response()->json($data);
    }




    public function saveData(Request $request)
    {
        $data = [
            "remarks" => 'error',
            "message" => '',
            "data" => [],
        ];

        $user_id = Auth::id();
        $employee_id    = $request->employee_id;
        $inputFirstName = $request->inputFirstName;
        $inputLastName  = $request->inputLastName;
        $inputMiddleName = $request->inputMiddleName;
        $inputPhoneNo   = $request->inputPhoneNo;
        $inputPosition  = $request->inputPosition;
        $inputClientName = $request->inputClientName;
        $inputAreasName = $request->inputAreasName;


        DB::beginTransaction();
        try {

            $sql = "SELECT  phone_no
                    FROM employees
                    WHERE id <> ?
                        AND (
                            phone_no = ?
                        )
                    ";
            $validateDuplicate = DB::select($sql, [
                $employee_id,
                $inputPhoneNo,

            ]);
            if (count($validateDuplicate) > 0) {
                $data["remarks"] = "warning";
                $data["message"] = "Data already in database";
                if ($validateDuplicate[0]->phone_no == $inputPhoneNo) {
                    $data["data"]["inputErr"][] = "inputPhoneNo";
                }
                return response()->json($data);
            }

            if ($employee_id > 0) {
                DB::update(
                    'UPDATE employees
                    SET
                        first_name = ?,
                        last_name = ?,
                        middle_name = ?,
                        phone_no = ?,
                        position = ?,
                        areas_id=?,
                        client_id = ?,
                        modified_by = ?,
                        modified_at = ?
                    WHERE
                        id = ?
                    ',
                    [
                        $inputFirstName,
                        $inputLastName,
                        $inputMiddleName,
                        $inputPhoneNo,
                        $inputPosition,
                        $inputAreasName,
                        $inputClientName,
                        $user_id,
                        now(),

                        $employee_id,
                    ]
                );
                $data["message"] = "Data successfully saved in database!";
            } else {
                DB::insert(
                    'INSERT INTO employees
                    (first_name, last_name, middle_name, phone_no, position,areas_id, client_id, created_by)
                    VALUES
                    (?, ?, ?, ?, ?, ?, ?,?)',
                    [
                        $inputFirstName,
                        $inputLastName,
                        $inputMiddleName,
                        $inputPhoneNo,
                        $inputPosition,
                        $inputAreasName,
                        $inputClientName,
                        $user_id  // Remove the trailing comma here
                    ]
                );

                $data["message"] = "Data successfully inserted in database!";
            }

            $data["remarks"] = "success";
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            $data["remarks"] = "error";
            $data["message"] = print_r($e);
        }

        return response()->json($data);
    }


    public function deleteData(Request $request)
    {
        $data = [
            "remarks" => 'error',
            "message" => '',
            "data" => [],
        ];

        $user_id = Auth::id();

        $employee_id = $request->employee_id;

        DB::beginTransaction();
        try {
            DB::update(
                'UPDATE employees
                    SET
                        is_deleted = ?,
                        modified_by = ?
                    WHERE
                        id = ?
                    ',
                [
                    1,
                    $user_id,
                    $employee_id,
                ]
            );

            $data["remarks"] = "success";
            $data["message"] = "Data successfully tag deleted in database!";
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            $data["remarks"] = "error";
            $data["message"] = print_r($e);
        }

        return response()->json($data);
    }

    public function minData(Request $request)
    {
        $data = [
            "remarks" => 'error',
            "message" => 'Something went wrong', // Corrected the key here
            "data" => [],
        ];

        $sql  = "SELECT areas.id, areas.name
        FROM areas
        WHERE areas.is_deleted = 0
        ORDER BY areas.name;";


        $data["data"]["items"] = DB::select($sql);

        if (count($data["data"]["items"]) > 0) {
            $data["remarks"] = "success";
            $data["message"] = "Data found successfully!"; // Corrected the key here
        }

        return response()->json($data);
    }

    public function EmpminData(Request $request)
    {
        $data = [
            "remarks" => 'error',
            "message" => 'Something went wrong',
            "data" => [],
        ];

        $clientId = $request->clientId;

        $sql = "SELECT *
                FROM employees
                WHERE client_id = ? and is_deleted = 0 order by last_name asc";

        $data["data"]["items"] = DB::select($sql, [$clientId]);

        if (count($data["data"]["items"]) > 0) {
            $data["remarks"] = "success";
            $data["message"] = "Data found successfully!";
        }

        return response()->json($data);
    }

  
}