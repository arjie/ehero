<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TicketEarnController extends Controller
{
    public function index()
    {
        return view('ticketearn');
    }

    public function validateFields($field, $ret = null)
    {
        if (
            $field === 0
            || $field === "0"
        ) {
            return $field;
        }
        return ($field) ? $field : $ret;
    }

    public function loadItems(Request $request)
    {
        $clientId = $request->input('clientId');
        $employeeId = $request->input('employeeId');
    
        $data = [
            "remarks" => 'error',
            "message" => 'Something went wrong',
            "data" => [],
        ];
    
        try {
            $query = DB::table('ticket_earn as c')
                        ->select('c.id as catid', 'c.ticket_earn', 'e.id', 'e.first_name', 'e.last_name', 'e.middle_name', 'e.phone_no', 'e.position', 'c.created_at',
                                  'ct.generator_code as categCode', 'ct.name as categname',
                                  'cl.name as clientname', 'cl.code as clientCode',
                                  'ar.code as areasCode','c.verified_at')
                        ->join('employees as e', 'e.id', '=', 'c.employee_id')
                        ->join('categories as ct', 'ct.id', '=', 'c.category_id')
                        ->join('clients as cl', 'cl.id', '=', 'e.client_id')
                        ->join('areas as ar', 'ar.id', '=', 'e.areas_id')
                        ->where('c.is_deleted', 0)
                        ->where('c.verified_at', 0);
    
            // Add conditions for filtering by client and employee IDs if provided
            if ($clientId !== null) {
                $query->where('cl.id', $clientId);
            }
    
            if ($employeeId !== null) {
                $query->where('e.id', $employeeId);
            }
    
            $query->orderBy('c.created_at', 'DESC');
    
            $data["data"]["items"] = $query->get()->toArray();
    
            if (!empty($data["data"]["items"])) {
                $data["remarks"] = "success";
                $data["message"] = "Data found successfully!";
            } else {
                $data["message"] = "No data found!";
            }
        } catch (\Exception $e) {
            $data["message"] = $e->getMessage();
        }
    
        return response()->json($data);
    }
    


    public function loadItemsVerified(Request $request)
    {
        $clientId = $request->input('clientId');
        $employeeId = $request->input('employeeId');
    
    
        $data = [
            "remarks" => 'error',
            "message" => 'Something went wrong',
            "data" => [],
        ];
    
        try {
            $query = DB::table('ticket_earn as c')
                        ->select('c.id as catid', 'c.ticket_earn', 'e.id', 'e.first_name', 'e.last_name', 'e.middle_name', 'e.phone_no', 'e.position', 'c.created_at',
                                  'ct.generator_code as categCode', 'ct.name as categname',
                                  'cl.name as clientname', 'cl.code as clientCode',
                                  'ar.code as areasCode','c.verified_at')
                        ->join('employees as e', 'e.id', '=', 'c.employee_id')
                        ->join('categories as ct', 'ct.id', '=', 'c.category_id')
                        ->join('clients as cl', 'cl.id', '=', 'e.client_id')
                        ->join('areas as ar', 'ar.id', '=', 'e.areas_id')
                        ->where('c.is_deleted', 0)
                        ->where('c.verified_at', 1);
                        if ($clientId !== null) {
                            $query->where('cl.id', $clientId);
                        }
                
                        if ($employeeId !== null) {
                            $query->where('e.id', $employeeId);
                        }
                

            $query->orderBy('c.created_at', 'DESC');
    
            $data["data"]["items"] = $query->get()->toArray();
    
            if (!empty($data["data"]["items"])) {
                $data["remarks"] = "success";
                $data["message"] = "Data found successfully!";
            } else {
                $data["message"] = "No data found!";
            }
        } catch (\Exception $e) {
            $data["message"] = $e->getMessage();
        }
    
        return response()->json($data);
    }
    

    
    /*public function loadItem(Request $request)
    {
        $data = [
            "remarks" => 'error',
            "message" => 'Something went wrong',
            "data" => [],
        ];

        $id = $request->employee_id;

        $sql  = "SELECT  e.id, e.first_name, e.last_name, e.middle_name, e.phone_no, e.position, c.name
        FROM employees e
        JOIN clients c ON e.client_id = c.id
        WHERE e.id = $id";
        $data["data"]["item"] = DB::select($sql);

        if (count($data["data"]["item"]) > 0) {
            $data["remarks"] = "success";
            $data["message"] = "Data found successfully!";
        }

        return response()->json($data);
    }
    */

    public function saveData(Request $request)
    {
        $data = [
            "remarks" => 'error',
            "message" => '',
            "data" => [],
        ];

        $user_id = Auth::id();
        $inputClientName = $request->inputClientName;
        $employee_ids = $request->selectedEmployees; // Changed variable name to be more accurate
        $transaction_code = date("Ymd") . uniqid();
        $categ_id = $request->Category;

        DB::beginTransaction();
        try {
            $sql  = "SELECT categories.id, categories.equivalent_no
                FROM categories
                WHERE categories.is_deleted = 0
                ORDER BY categories.name;";

            $category_values = DB::select($sql);

            $totalEmployees = count(array_unique($employee_ids)); // Count unique employees

            for ($e = 0; $e < count($employee_ids); $e++) {
                for ($i = 0; $i < count($categ_id); $i++) {
                    $defaultValue = 0;
                    for ($val = 0; $val < count($category_values); $val++) {
                        if ($categ_id[$i] == $category_values[$val]->id) {
                            $defaultValue = intval($category_values[$val]->equivalent_no);
                            break;
                        }
                    }

                    if ($defaultValue > 0) {
                        for ($j = 0; $j < $defaultValue; $j++) {
                            do {
                                $randTicket = $this->generateRandomTicket(8);
                            } while ($this->isTicketDuplicate($randTicket));

                            DB::insert(
                                'INSERT INTO ticket_earn
                                    (
                                        `employee_id`, `category_id`, `ticket_earn`, `transaction_code`, `created_by`
                                    )
                                    VALUES
                                    (?, ?, ?, ?, ?)',
                                [
                                    $employee_ids[$e],
                                    $categ_id[$i],
                                    $randTicket,
                                    $transaction_code,
                                    $user_id
                                ]
                            );
                        }
                    }
                }
            }

            $data["message"] = "Employees were successfully inserted in the database!";
            $data["remarks"] = "success";
            $data["totalEmployees"] = $totalEmployees;

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            $data["remarks"] = "error";
            $data["message"] = $e->getMessage();
        }

        return response()->json($data);
    }


    
    



    public function generateRandomTicket($length)
    {
        //validation of duplicate

        $min = pow(10, $length - 1);
        $max = pow(10, $length) - 1;

        return mt_rand($min, $max);
    }
    private function isTicketDuplicate($ticket)
    {
        // Check if the ticket number already exists in the database
        // Replace 'ticket_earn' with your actual table name
        $existingTicket = DB::table('ticket_earn')->where('ticket_earn', $ticket)->first();

        return !empty($existingTicket);
    }



    

    public function EmpminDataEarn(Request $request)
    {
        $data = [
            "remarks" => 'error',
            "message" => 'Something went wrong',
            "data" => [],
        ];

        $clientId = $request->clientId;

        $sql = "SELECT *
                FROM employees
                WHERE client_id = ? and is_deleted = 0 order by last_name asc";

        $data["data"]["items"] = DB::select($sql, [$clientId]);

        if (count($data["data"]["items"]) > 0) {
            $data["remarks"] = "success";
            $data["message"] = "Data found successfully!";
        }

        return response()->json($data);
    }





    public function UpdateData(Request $request)
    {
        $data = [
            "remarks" => "error",
            "message" => "",
            "data" => [],
        ];
        
        $user_id = Auth::id();
        $selectedValues = json_decode($request->selectedValues, true); // Decode JSON string to array
        
        try {
            DB::beginTransaction();
        
            // Loop through each selected ID and update the corresponding record in the database
            foreach ($selectedValues as $ticket_id) {
                if (!empty($ticket_id)) { // Check if $employee_id is not empty
                    // Use DB facade's update method to update the record
                    DB::table('ticket_earn')
                        ->where('id', $ticket_id)
                        ->update([
                            'verified_at' => 1, // Assuming you want to set verified_at to 1
                            'verified_by' => $user_id
                        ]);
                }
            }
        
            // If all updates are successful, commit the transaction
            DB::commit();
        
            // If no exception occurred, set success response
            $data["remarks"] = "success";
            $data["message"] = "Data successfully updated in the database!";
        } catch (\Exception $e) {
            // If an exception occurred during updates, rollback the transaction
            DB::rollback();
        
            // Set error message in case of exception
            $data["message"] = $e->getMessage(); // Log or return specific error message
        }
        
        // Return JSON response indicating success or failure
        return response()->json($data);
        
    }
    
}