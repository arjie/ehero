<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ClientsController;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\PositionsController;
use App\Http\Controllers\PRFsController;
use App\Http\Controllers\EmployeesController;
use App\Http\Controllers\GenerateCodeController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\TicketEarnController;



/*
GenerateCodeController
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::middleware(['auth', 'check_profile'])->group(function () {


    
Route::get('/', function () {
    return view('dashboard');
});


    Route::post('/CLIENT-load-mindataDash', [DashboardController::class, 'minDataDash']);
    Route::post('/TicketEarn-load', [DashboardController::class, 'TicketEarn']);
    Route::post('/TicketEarn-load-Top', [DashboardController::class, 'TopTicketEarn']);


    Route::get('/clients', [ClientsController::class, 'index']);
    Route::post('/CLIENT-load-item', [ClientsController::class, 'loadItem']);
    Route::post('/CLIENT-load-items', [ClientsController::class, 'loadItems']);
    Route::post('/CLIENT-save-data', [ClientsController::class, 'saveData']);
    Route::post('/CLIENT-delete-data', [ClientsController::class, 'deleteData']);
    Route::post('/CLIENT-load-mindata', [ClientsController::class, 'minData']);



    Route::get('/categories', [CategoriesController::class, 'index']);
    Route::post('/CATEGORY-load-item', [CategoriesController::class, 'loadItem']);
    Route::post('/CATEGORY-load-items', [CategoriesController::class, 'loadItems']);
    Route::post('/CATEGORY-save-data', [CategoriesController::class, 'saveData']);
    Route::post('/CATEGORY-delete-data', [CategoriesController::class, 'deleteData']);
    Route::post('/CATEGORY-load-mindata', [CategoriesController::class, 'getCategories']);


    Route::get('/positions', [PositionsController::class, 'index']);
    Route::post('/POSITION-load-item', [PositionsController::class, 'loadItem']);
    Route::post('/POSITION-load-items', [PositionsController::class, 'loadItems']);
    Route::post('/POSITION-save-data', [PositionsController::class, 'saveData']);
    Route::post('/POSITION-delete-data', [PositionsController::class, 'deleteData']);

    Route::get('/prfs', [PRFsController::class, 'index']);
    Route::post('/PRF-load-item', [PRFsController::class, 'loadItem']);
    Route::post('/PRF-load-items', [PRFsController::class, 'loadItems']);
    Route::post('/PRF-save-data', [PRFsController::class, 'saveData']);
    Route::post('/PRF-delete-data', [PRFsController::class, 'deleteData']);

    Route::get('/employees', [EmployeesController::class, 'index']);
    Route::post('/EMPLOYEE-load-item', [EmployeesController::class, 'loadItem']);
    Route::post('/EMPLOYEE-load-items', [EmployeesController::class, 'loadItems']);
    Route::post('/EMPLOYEE-save-data', [EmployeesController::class, 'saveData']);
    Route::post('/EMPLOYEE-delete-data', [EmployeesController::class, 'deleteData']);

    Route::post('/EMPLOYEE-load-mindata', [EmployeesController::class, 'EmpminData']);

    Route::post('/EMPLOYEE-load-mindata-earn', [EmployeesController::class, 'EmpminDataEarn']);






    Route::post('/AREAS-load-mindata', [EmployeesController::class, 'minData']);

    Route::get('/generatecode', [GenerateCodeController::class, 'index']);
    Route::post('/GENERATECODE-load-item', [GenerateCodeController::class, 'loadItem']);
    Route::post('/GENERATECODE-load-items', [GenerateCodeController::class, 'loadItems']);
    Route::post('/GENERATECODE-save-data', [GenerateCodeController::class, 'saveData']);
    Route::post('/GENERATECODE-delete-data', [GenerateCodeController::class, 'deleteData']);






    Route::get('/ticketearn', [TicketEarnController::class, 'index']);
    Route::post('/TICKETEARN-load-item', [TicketEarnController::class, 'loadItem']);
    Route::post('/TICKETEARN-load-items', [TicketEarnController::class, 'loadItems']);
    Route::post('/TICKETEARN-save-data', [TicketEarnController::class, 'saveData']);
    Route::post('/TICKETEARN-update-data', [TicketEarnController::class, 'UpdateData']);
    Route::post('/EMPLOYEE-load-mindata-earn', [TicketEarnController::class, 'EmpminDataEarn']);
    Route::post('/TICKETEARN-load-verified-items', [TicketEarnController::class, 'loadItemsVerified']);
});




Route::get('/test-database', function () {
    // phpinfo();
    try {
        DB::connection()->getPdo();
        return 'Connected to the database!';
    } catch (\Exception $e) {
        return 'Database connection failed: ' . $e->getMessage();
    }
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('dashboard');